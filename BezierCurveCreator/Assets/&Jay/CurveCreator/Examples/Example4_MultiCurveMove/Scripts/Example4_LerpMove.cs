﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Example4_LerpMove : MonoBehaviour {

	public BezierLerp lerpCurve;
	public float moveSpeed=0.2f;
	void Update () 
	{
		lerpCurve.lerpT = Mathf.PingPong (Time.time *moveSpeed, 1f);	
	}
}
