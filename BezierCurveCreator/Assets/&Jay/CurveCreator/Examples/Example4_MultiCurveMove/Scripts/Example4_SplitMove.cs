﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Example4_SplitMove : MonoBehaviour {

	public BezierSplit splitCurve;
	public float moveSpeed=0.2f;

	void Update () 
	{
		splitCurve.endT = 0.5f + Mathf.PingPong (Time.time*moveSpeed, 0.5f);	
	}
}
