﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Example2_DataView : MonoBehaviour {

	public BezierCurve bezierCurve;
	public Pointer pointer;
	public TextMesh text_Lenght,text_ActiveTrigger,text_Position,text_Velocity,text_Acceleration;
	public GameObject intersectionPlane;
	public TextMesh text_Intersection;

	// Update is called once per frame
	void Update () 
	{
		text_Lenght.text = "Curve length: " + bezierCurve.GetLength (1f).ToString("F1");

		text_Position.text = "Pointer global position: " + bezierCurve.GetPosition (pointer.t);

		text_Velocity.text = "Pointer global velocity: " + bezierCurve.GetVelocity (pointer.t);

		text_Acceleration.text = "Pointer global acceleration: " + bezierCurve.GetAcceleration (pointer.t);

		text_Intersection.text = "Intersection with Plane: ";
		foreach (var item in bezierCurve.IntersectionPlane(intersectionPlane.transform.position,intersectionPlane.transform.up)) 
		{
			text_Intersection.text += item.ToString ("F2") + ", ";
		}

		var activeTriggers = bezierCurve.GetActiveTriggers (pointer.t);
		if (activeTriggers.Length > 0)
			text_ActiveTrigger.text = "Active Curve Trigger: " + activeTriggers [0].name;
		else
			text_ActiveTrigger.text = "Active Curve Trigger:";
		
	}
}
