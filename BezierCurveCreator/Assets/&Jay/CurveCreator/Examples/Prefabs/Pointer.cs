﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pointer : MonoBehaviour 
{

	public BezierCurve curve;
	public float speed = 1f;
	public bool useSingleCurve = true;
	[HideInInspector]
	public float t;

	void Update () 
	{
		if (useSingleCurve)
			SingleCurveMove ();
		else
			MultiCurveMove ();
	}

	void SingleCurveMove()
	{
		t += speed * Time.deltaTime;
		t = Mathf.Repeat (t, 1f);
		transform.position = curve.GetPosition(t);
		transform.rotation = Quaternion.LookRotation (curve.GetForward (t), curve.GetUp (t));
	}

	void MultiCurveMove()
	{
		t += speed * Time.deltaTime/curve.GetVelocity(t).magnitude;
		if (t >= 1) 
		{
			if(curve.nextCurves.Count > 0)
				curve = curve.nextCurves [Random.Range (0, curve.nextCurves.Count)];
		}
		t = Mathf.Repeat (t, 1f);
		transform.position = curve.GetPosition(t);
		transform.rotation = Quaternion.LookRotation (curve.GetForward (t), curve.GetUp (t));
	}
}
