﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Example3_ConstTChange : MonoBehaviour {

	public BezierCurve curve;
	public float speed = 0.5f;
	float t;

	void Update () 
	{
		t += speed * Time.deltaTime;
		t = Mathf.Repeat (t, 1f);
		transform.position = curve.GetPosition(t);
		transform.rotation = Quaternion.LookRotation (curve.GetForward (t), curve.GetUp (t));
	}
}
