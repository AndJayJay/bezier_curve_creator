﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Example3_ConstPositionChange : MonoBehaviour {

	public BezierCurve curve;
	public float speed = 0.5f;
	float t;

	void Update () 
	{
		t += speed * Time.deltaTime/curve.GetVelocity(t).magnitude;
		t = Mathf.Repeat (t, 1f);
		transform.position = curve.GetPosition(t);
		transform.rotation = Quaternion.LookRotation (curve.GetForward (t), curve.GetUp (t));
	}
}
