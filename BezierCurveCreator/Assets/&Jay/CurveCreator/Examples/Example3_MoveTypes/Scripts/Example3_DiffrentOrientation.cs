﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Example3_DiffrentOrientation : MonoBehaviour {

	public enum MoveType {Position,PositionAndDirection,PositionAndDirectionAndRotation}

	public BezierCurve curve;
	public MoveType movetype = MoveType.Position;
	public float speed = 0.5f;
	public float t;

	void Update () 
	{
		t += speed * Time.deltaTime;
		t = Mathf.Repeat (t, 1f);
		transform.position = curve.GetPosition(t);
		if(movetype==MoveType.PositionAndDirection)
			transform.rotation = Quaternion.LookRotation (curve.GetForward (t));
		if(movetype==MoveType.PositionAndDirectionAndRotation)
			transform.rotation = Quaternion.LookRotation (curve.GetForward (t),curve.GetUp(t));
	}
}