﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
#pragma warning disable 0809

/// <summary>
/// Class for curve created from fragment of relative curve.
/// </summary>
public class BezierSplit : BezierSingle 
{
	#region Attributes
	/// <summary>
	/// Force rotation flag. If rotation is forced, it use local manual rotation. Else it use rotation from relative curve.
	/// </summary>
	public bool forceRotation = false;

	/// <summary>
	/// Transform of calculate data for split.
	/// </summary>
	public BezierSpace space = BezierSpace.Local;

	/// <summary>
	/// Relative curve.
	/// </summary>
	public BezierSingle relatedCurve = null;

	/// <summary>
	/// Curve data for verify changes.
	/// </summary>
	Bezier verifiedCurve = new Bezier();
	#endregion

	#region Properties

	[SerializeField]
	private float _startT = 0f;
	/// <summary>
	/// Start point of split [0,1].
	/// </summary>
	public float startT
	{
		get
		{
			return _startT;
		}
		set 
		{
			if (value != _startT) 
			{				
				if (value >= endT)
					_startT = endT;
				else
					_startT = value;

				CalculateCurve();
			}
		}
	}

	[SerializeField]
	private float _endT = 1f;
	/// <summary>
	/// End point of split [0,1].
	/// </summary>
	public float endT
	{
		get
		{
			return _endT;
		}
		set 
		{
			if (value != _endT) 
			{
				if (value <= startT)
					_endT = startT;
				else
					_endT = value;

				CalculateCurve();
			}
		}
	}
	#endregion

	#region Methods
	/// <summary>
	/// Calculate fragment of relative curve.
	/// </summary>
	void CalculateCurve()
	{
		if (startT == endT) 
		{
			Vector3 position = verifiedCurve.GetPoint(startT);
			curve = new Bezier (position, position, position, position);
		} 
		else 
		{
			Vector3 temp = Vector3.Lerp(verifiedCurve.startNode.referencePosition, verifiedCurve.endNode.referencePosition, startT);
			Vector3 tempEndRefPos = Vector3.Lerp(verifiedCurve.endNode.referencePosition, verifiedCurve.endNode.mainPosition, startT);
			Vector3 tempStartRefPos = Vector3.Lerp(temp, tempEndRefPos, startT);
			Vector3 tempStartMainPos = verifiedCurve.GetPoint (startT);
			Vector3 tempEndMainPos = verifiedCurve.endNode.mainPosition;

			float t = (endT - startT)/(1f-startT);

			Vector3 newStartMainPos = tempStartMainPos;
			Vector3 newStartRefPos = Vector3.Lerp(tempStartMainPos, tempStartRefPos, t);
			temp = Vector3.Lerp(tempStartRefPos, tempEndRefPos, t);
			Vector3 newEndRefPos = Vector3.Lerp(newStartRefPos, temp, t);

			Vector3 a,b,c,d;
			a = -tempStartMainPos + 3f * tempStartRefPos - 3f * tempEndRefPos + tempEndMainPos;
			b = 3f * tempStartMainPos - 6f * tempStartRefPos + 3f * tempEndRefPos;
			c = -3f * tempStartMainPos + 3f * tempStartRefPos;
			d = tempStartMainPos;
			Vector3 newEndMainPos =  t * t * t * a + t * t * b + t * c + d;

			curve = new Bezier (newStartMainPos, newStartRefPos, newEndRefPos, newEndMainPos);
		}
	}

	/// <summary>
	/// Create classic bezier curve component from split curve component. New component is automaticly added to GameObject.
	/// </summary>
	/// <param name="autoDestroy">If true, destroy this component from GameObject, after conversion.</param>
	/// <returns>Component of converted bezier curve</returns>
	public BezierSingle ConvertToBezierSingle(bool autoDestroy)
	{
		VerifyCurve ();
		var temp = new GameObject ();
		temp.transform.SetParent (transform.parent);
		temp.name = name+"_BezierSingle";
		BezierSingle newCurve = temp.AddComponent<BezierSingle> ();
		newCurve.SetCurve (curve.Copy());
		if(autoDestroy)
			DestroyImmediate (this);
		return newCurve;
	}

	/// <summary>
	/// Get rotation around curve in selected point. If forceRotation is false, then is used rotation from relative curve.
	/// Else this method return local value of rotation.
	/// </summary>
	/// <param name="t">Position on curve [0,1]</param>
	/// <returns>Rotation in angles.</returns>
	public override float GetRotation(float t)
	{
		if(autoUpdate || !Application.isPlaying)
			VerifyCurve ();
		
		if (forceRotation)
			return base.GetRotation (t);
		else 
		{
			t = t * (endT - startT) + startT;
			if (relatedCurve == null)
				return 0f;
			else
				return relatedCurve.GetRotation (t);
		}
	}

	[Obsolete("SetCurve is not supported for lerp curve. Curve is auto generated.")]
	public override void Reverse(){	}

	[Obsolete("SetCurve is not supported for split curve")]
	public override void SetCurve(Bezier curve){}

	/// <summary>
	/// Update curve, if relative curve is changed.
	/// </summary>
	public override void VerifyCurve()
	{
		if (relatedCurve != null) 
		{		
			Bezier tempCurve = relatedCurve.GetCurve ();

			if (space == BezierSpace.World)
				tempCurve = tempCurve.TransformBezier (relatedCurve.transform);
			if (space == BezierSpace.Self)
				tempCurve = tempCurve.TransformBezier (relatedCurve.transform, transform);

			if (!verifiedCurve.Compare (tempCurve)) 
			{					
				verifiedCurve = tempCurve.Copy ();
				CalculateCurve ();
			}
		} 
	}

	#endregion

}
