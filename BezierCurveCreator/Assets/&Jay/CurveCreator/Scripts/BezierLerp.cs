﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
#pragma warning disable 0809

/// <summary>
/// Class for interpolated curve between two relative curves.
/// </summary>
public class BezierLerp : BezierSingle {

	#region Enums
	/// <summary>
	/// Type of interpolation.
	/// </summary>
	public enum LerpType {
		/// <summary>
		/// Linear interpolation.
		/// </summary>
		Linear,
		/// <summary>
		/// Spherical interpolation.
		/// </summary>
		Spherical
	};

	/// <summary>
	/// Type of interpolation direction.
	/// </summary>
	public enum LerpDirection {
		/// <summary>
		/// Interpolate between start curve and end curve.
		/// </summary>
		StartToEnd,

		/// <summary>
		/// Interpolate between start curve and end curve. Interpolation for end node is reversed.
		/// </summary>
		Cross,

		/// <summary>
		/// Interpolate between start nodes of relative curves and end nodes.
		/// </summary>
		SideToSide,

		/// <summary>
		/// Interpolate between start nodes of relative curves and end nodes. Interpolation for end node is reversed.
		/// </summary>
		SideCross
	};
	#endregion

	#region Attributes
	/// <summary>
	/// End relative curve.
	/// </summary>
	public BezierSingle endCurve = null;

	/// <summary>
	/// Force rotation flag. If rotation is forced, it use local manual rotation. Else it interpolate rotation from relative curves.
	/// </summary>
	public bool forceRotation = false;

	/// <summary>
	/// Transform of calculate data for interpolation.
	/// </summary>
	public BezierSpace space = BezierSpace.Local;

	/// <summary>
	/// Start relative curve.
	/// </summary>
	public BezierSingle startCurve = null;

	/// <summary>
	/// Start curve data for verify changes.
	/// </summary>
	private Bezier verifiedCurveStart = new Bezier();

	/// <summary>
	/// End curve data for verify changes.
	/// </summary>
	private Bezier verifiedCurveEnd = new Bezier();

	#endregion

	#region Properties

	[SerializeField]
	private LerpType _lerpType = LerpType.Linear;
	/// <summary>
	/// Selected interpolation type.
	/// </summary>
	public LerpType lerpType
	{
		get 
		{
			return _lerpType;
		}
		set 
		{
			if (lerpType != value) 
			{
				_lerpType = value;
				CalculateCurve ();
			}
		}
	}

	[SerializeField]
	private LerpDirection _lerpDirection = LerpDirection.StartToEnd;
	/// <summary>
	/// Selected interpolation direction.
	/// </summary>
	public LerpDirection lerpDirection
	{
		get 
		{
			return _lerpDirection;
		}
		set 
		{
			if (_lerpDirection != value) 
			{
				_lerpDirection = value;
				CalculateCurve ();
			}
		}
	}

	[SerializeField]
	private float _lerpT = 0f;
	/// <summary>
	/// Weight of interpolation between relative curves [0,1].
	/// </summary>
	public float lerpT
	{
		get
		{
			return _lerpT;
		}
		set 
		{
			if (value != _lerpT) 
			{				
				if (value > 1f)
					_lerpT = 1f;
				else
					if (value < 0f)
						_lerpT = 0f;
					else
						_lerpT = value;

				CalculateCurve();
			}
		}
	}

	#endregion

	#region Methods
	/// <summary>
	/// Calculate interpolated curve.
	/// </summary>
	void CalculateCurve()
	{
		BezierNode tempStartNode = null;
		BezierNode tempEndNode = null;

		switch (lerpType)
		{
		case LerpType.Linear:
			{
				switch (lerpDirection) 
				{
				case LerpDirection.StartToEnd:
					{
						tempStartNode = BezierNode.Lerp (verifiedCurveStart.startNode, verifiedCurveEnd.startNode, lerpT);
						tempEndNode = BezierNode.Lerp (verifiedCurveStart.endNode, verifiedCurveEnd.endNode, lerpT);
						break;
					}
				case LerpDirection.Cross:
					{
						tempStartNode = BezierNode.Lerp (verifiedCurveStart.startNode, verifiedCurveEnd.startNode, lerpT);
						tempEndNode = BezierNode.Lerp (verifiedCurveEnd.endNode,verifiedCurveStart.endNode, lerpT);
						break;
					}
				case LerpDirection.SideCross:
					{
						tempStartNode = BezierNode.Lerp (verifiedCurveStart.startNode, verifiedCurveStart.endNode, lerpT);
						tempEndNode = BezierNode.Lerp (verifiedCurveEnd.endNode,verifiedCurveEnd.startNode, lerpT);
						break;
					}
				case LerpDirection.SideToSide:
					{
						tempStartNode = BezierNode.Lerp (verifiedCurveStart.startNode, verifiedCurveStart.endNode, lerpT);

						tempEndNode = BezierNode.Lerp (verifiedCurveEnd.startNode,verifiedCurveEnd.endNode, lerpT);
						break;
					}
				}
				break;
			}
		case LerpType.Spherical:
			{
				switch (lerpDirection) 
				{
				case LerpDirection.StartToEnd:
					{
						tempStartNode = BezierNode.Slerp (verifiedCurveStart.startNode, verifiedCurveEnd.startNode, lerpT);
						tempEndNode = BezierNode.Slerp (verifiedCurveStart.endNode, verifiedCurveEnd.endNode, lerpT);
						break;
					}
				case LerpDirection.Cross:
					{
						tempStartNode = BezierNode.Slerp (verifiedCurveStart.startNode, verifiedCurveEnd.startNode, lerpT);
						tempEndNode = BezierNode.Slerp (verifiedCurveEnd.endNode,verifiedCurveStart.endNode, lerpT);
						break;
					}
				case LerpDirection.SideCross:
					{
						tempStartNode = BezierNode.Slerp (verifiedCurveStart.startNode, verifiedCurveStart.endNode, lerpT);
						tempEndNode = BezierNode.Slerp (verifiedCurveEnd.endNode,verifiedCurveEnd.startNode, lerpT);
						break;
					}
				case LerpDirection.SideToSide:
					{
						tempStartNode = BezierNode.Slerp (verifiedCurveStart.startNode, verifiedCurveStart.endNode, lerpT);
						tempEndNode = BezierNode.Slerp (verifiedCurveEnd.startNode,verifiedCurveEnd.endNode, lerpT);
						break;
					}
				}
				break;
			}
		}

		curve.startNode = tempStartNode;
		curve.endNode = tempEndNode;
	}

	/// <summary>
	/// Create classic bezier curve component from interpolated curve component. New component is automaticly added to GameObject.
	/// </summary>
	/// <param name="autoDestroy">If true, destroy this component from GameObject, after conversion.</param>
	/// <returns>Component of converted bezier curve</returns> 
	public BezierSingle ConvertToBezierSingle(bool autoDestroy)
	{
		VerifyCurve ();

		var temp = new GameObject ();
		temp.transform.SetParent (transform.parent);
		temp.name = name+"_BezierSingle";
		BezierSingle newCurve = temp.AddComponent<BezierSingle> ();
		newCurve.SetCurve (curve.Copy());
		if(autoDestroy)
			DestroyImmediate (this);
		return newCurve;
	}

	/// <summary>
	/// Get rotation around curve in selected point. If forceRotation is false, then rotation is interpolated from relative curves.
	/// Else this method return local value of rotation.
	/// </summary>
	/// <param name="t">Position on curve [0,1]</param>
	/// <returns>Rotation in angles.</returns>
	public override float GetRotation(float t)
	{
		if(autoUpdate || !Application.isPlaying)
			VerifyCurve ();

		if (forceRotation)
			return base.GetRotation (t);
		else 
		{
			if (startCurve != null && endCurve != null)
				return Mathf.Lerp (startCurve.GetRotation (t), endCurve.GetRotation (t), lerpT);
			else
				return 0f;
		}
	}

	[Obsolete("SetCurve is not supported for lerp curve. Curve is auto generated.")]
	public override void Reverse(){	}

	[Obsolete("SetCurve is not supported for lerp curve. Curve is auto generated.")]
	public override void SetCurve(Bezier curve){}

	/// <summary>
	/// Update curve, if relative curves are changed.
	/// </summary>
	public override void VerifyCurve()
	{
		if (startCurve != null && endCurve != null) 
		{	
			Bezier tempCurveStart = startCurve.GetCurve();
			Bezier tempCurveEnd = endCurve.GetCurve();

			if (space == BezierSpace.World) 
			{
				tempCurveStart = tempCurveStart.TransformBezier(startCurve.transform);
				tempCurveEnd = tempCurveEnd.TransformBezier(endCurve.transform);
			} 
			if (space == BezierSpace.Self) 
			{
				tempCurveStart = tempCurveStart.TransformBezier(startCurve.transform,transform);
				tempCurveEnd = tempCurveEnd.TransformBezier(endCurve.transform,transform);
			} 

			bool verificationStatus = true;

			if (!verifiedCurveStart.Compare (tempCurveStart)) 
			{
				verifiedCurveStart = tempCurveStart.Copy ();
				verificationStatus = false;
			}
			if (!verifiedCurveEnd.Compare (tempCurveEnd)) 
			{
				verifiedCurveEnd =tempCurveEnd.Copy ();
				verificationStatus = false;
			}
			if(!verificationStatus)
				CalculateCurve ();
		} 
		else 
		{
			verifiedCurveStart = new Bezier();
			verifiedCurveEnd = new Bezier();
			CalculateCurve ();
		}
	}

	#endregion

}
