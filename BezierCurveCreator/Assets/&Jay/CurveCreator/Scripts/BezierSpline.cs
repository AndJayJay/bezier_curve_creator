﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
#pragma warning disable 0809

/// <summary>
/// Class for control group of curves as single curve.
/// </summary>
public class BezierSpline : BezierCurve 
{
	#region Attributes
	/// <summary>
	/// If true, ignore local triggers of curves and use own triggers list.
	/// </summary>
	public bool forceTriggers = false;
	#endregion

	#region Properties
	[SerializeField]
	private List<BezierCurve> _curves = new List<BezierCurve>();
	/// <summary>
	/// List of curves in use. The order in list corresponds to the order of using curves for spline.
	/// </summary>
	public List<BezierCurve> curves
	{
		get 
		{
			return _curves;
		}
		set 
		{
			if (value != _curves) 
			{
				//if (!BezierCurve.InfinityLoopConnection (this,curves))
				_curves = value;
			}
		}
	}

	#endregion 

	#region Methods

	/// <summary>
	/// Return curve corresponds to the selected value of spline curve parameter.
	/// </summary>
	/// <param name="t">Position on spline curve [0,1].</param>
	/// <param name="restT">Position in local space of founded curve.</param>
	/// <param name="t">Curve corresponds to the selected value of spline curve parameter.</param>
	/// <returns>True if curve is founded. Else return false.</returns>
	bool FindCurve (float t, out float restT,out BezierCurve curve)
	{
		restT = 0f;
		curve = null;

		if (curves.Count == 0) 
		{
			restT = 0f;
			return false;
		}
		if (t == 1f) 
		{
			restT = 1f;
			curve = curves [curves.Count - 1];
			return true;
		}
		if (t == 0f) 
		{
			restT = 0f;
			curve = curves [0];
			return true;
		}

		float step = 1f / ((float)curves.Count);
		restT = Mathf.Repeat (t, step);
		curve = curves [Mathf.RoundToInt((t - restT) / (step))];
		restT *= curves.Count; 
		return true;
	}

	/// <summary>
	/// Get acceleration in selected point of curve. Calculated from the corresponding curve.
	/// </summary>
	/// <param name="t">Position on curve [0,1]</param>
	/// <returns>Acceleration vector in global transform.</returns>
	public override Vector3 GetAcceleration(float t)
	{
		t = Mathf.Clamp01(t);
		float restT;
		BezierCurve lastCurve;

		if (!FindCurve (t, out restT,out lastCurve))
			return Vector3.zero;

		if(lastCurve == null)
			return Vector3.zero;

		return lastCurve.GetAcceleration (restT);
	}

	/// <summary>
	/// Get end node of last curve.
	/// </summary>
	/// <returns>End node of curve.</returns>
	public override BezierNode GetEndNode()
	{
		if (curves.Count > 0)
			return curves [curves.Count-1].GetEndNode();
		else
			return new BezierNode ();
	}

	/// <summary>
	/// Get forward direction in selected point of curve. Calculated from the corresponding curve.
	/// </summary>
	/// <param name="t">Position on curve [0,1]</param>
	/// <returns>Forward direction vector in global transform.</returns>
	public override Vector3 GetForward(float t)
	{
		t = Mathf.Clamp01(t);
		float restT;
		BezierCurve lastCurve;

		if (!FindCurve (t, out restT,out lastCurve))
			return Vector3.zero;

		if(lastCurve == null)
			return Vector3.zero;

		return lastCurve.GetForward (restT);
	}

	/// <summary>
	/// Get distance length from start to selected point on spline curve. Calculate as sum of internal curves lengths.
	/// Arc length is calculate with Legendre-Gauss quadrature. It is approximation method, but gets good accuracy with great efficiency.
	/// </summary>
	/// <param name="t">Position on spline curve [0,1]</param>
	/// <returns>Length of curve element.</returns>
	public override float GetLength (float t)
	{
		float restT;
		BezierCurve lastCurve;

		if (!FindCurve (t, out restT,out lastCurve))
			return 0f;

		float length = 0f;
		int count = curves.IndexOf (lastCurve);

		for (int i = 0; i < count; i++) 
		{
			if (curves [i] != null)
				length += curves [i].GetLength (1f);
		}

		if(lastCurve != null)			
			length += lastCurve.GetLength(restT);

		return length;
	}

	/// <summary>
	/// Get position in selected point of curve. Calculated from the corresponding curve.
	/// </summary>
	/// <param name="t">Position on curve [0,1]</param>
	/// <returns>Point coordinates in global transform.</returns>
	public override Vector3 GetPosition(float t)
	{
		t = Mathf.Clamp01(t);
		float restT;
		BezierCurve lastCurve;

		if (!FindCurve (t, out restT,out lastCurve))
			return Vector3.zero;

		if(lastCurve == null)
			return Vector3.zero;

		return lastCurve.GetPosition (restT);
	}

	/// <summary>
	/// Get rotation around curve in selected point. Calculated from the corresponding curve.
	/// </summary>
	/// <param name="t">Position on curve [0,1]</param>
	/// <returns>Rotation in angles.</returns>
	public override float GetRotation(float t)
	{
		t = Mathf.Clamp01(t);
		float restT;
		BezierCurve lastCurve;

		if (!FindCurve (t, out restT,out lastCurve))
			return 0f;

		if(lastCurve == null)
			return 0f;

		return lastCurve.GetRotation (restT);
	}

	/// <summary>
	/// Get start node of first curve.
	/// </summary>
	/// <returns>Start node of curve.</returns>
	public override BezierNode GetStartNode()
	{
		if (curves.Count > 0)
			return curves [0].GetStartNode ();
		else
			return new BezierNode ();
	}

	/// <summary>
	/// Get up direction in selected point of curve. Calculated from the corresponding curve.
	/// </summary>
	/// <param name="t">Position on spline curve [0,1]</param>
	/// <param name="ignoreInflectionPoints">Ignore inflection points of curves. Improve rotation continuity.</param>
	/// <param name="ignoreRotation">Ignore manual rotation changes. Get clear up direction vector.</param>
	/// <returns>Up direction vector in global transform.</returns>
	public override Vector3 GetUp(float t,bool ignoreInflectionPoints = false, bool ignoreRotation = false)
	{
		t = Mathf.Clamp01(t);
		float restT;
		BezierCurve lastCurve;

		if (!FindCurve (t, out restT,out lastCurve))
			return Vector3.zero;

		if(lastCurve == null)
			return Vector3.zero;

		return lastCurve.GetUp (restT,ignoreInflectionPoints,ignoreRotation);
	}

	/// <summary>
	/// Get velocity in selected point of curve. Calculated from the corresponding curve.
	/// </summary>
	/// <param name="t">Position on curve [0,1]</param>
	/// <returns>Velocity vector in global transform.</returns>
	public override Vector3 GetVelocity(float t)
	{
		t = Mathf.Clamp01(t);
		float restT;
		BezierCurve lastCurve;

		if (!FindCurve (t, out restT,out lastCurve))
			return Vector3.zero;

		if(lastCurve == null)
			return Vector3.zero;

		return lastCurve.GetVelocity (restT);
	}

	public override List<float> IntersectionLine (Vector3 position, Vector3 direction,float accuracy = 0.01f, float floatAccuracy = 0.00001f)
	{
		int i = 0;
		int count = curves.Count;

		List<float> roots = new List<float> ();
		foreach (var item in curves) 
		{
			if (item != null) 
			{
				var tempRoots = item.IntersectionLine (position, direction,accuracy, floatAccuracy);
				foreach (var root in tempRoots)
					roots.Add ((root + i) / count);
			}				
			i++;
		}

		return roots;	
	}

	public override List<float> IntersectionPlane (Vector3 position, Vector3 normal, float floatAccuracy = 0.00001f)
	{
		int i = 0;
		int count = curves.Count;

		List<float> roots = new List<float> ();
		foreach (var item in curves) 
		{
			if (item != null) 
			{
				var tempRoots = item.IntersectionPlane (position, normal, floatAccuracy);
				foreach (var root in tempRoots)
					roots.Add ((root + i) / count);
			}				
			i++;
		}

		return roots;
	}

	[Obsolete("Reverse is not yet supported for Spline Curve.")]
	public override void Reverse(){	}

	#endregion
}
