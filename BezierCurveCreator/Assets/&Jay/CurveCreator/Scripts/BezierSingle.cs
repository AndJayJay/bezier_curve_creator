﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class for single quadratic bezier curve component.
/// </summary>
public class BezierSingle : BezierCurve 
{
	#region Attributes
	/// <summary>
	/// Object with curve data.
	/// </summary>
	[SerializeField]
	protected Bezier curve = new Bezier();

	/// <summary>
	/// Connection type of curve's start node.  
	/// </summary>
	public BezierConnectionType startConnectionType = BezierConnectionType.None;

	/// <summary>
	/// Connection point of curve's start node in relative curve.
	/// </summary>
	public BezierConnectionPoint startConnectionPoint = BezierConnectionPoint.End;

	/// <summary>
	/// Relative curve for connection curve's start node.
	/// </summary>
	public BezierCurve startConnectedCurve = null;

	/// <summary>
	/// Connection type of curve's end node.  
	/// </summary>
	public BezierConnectionType endConnectionType = BezierConnectionType.None;

	/// <summary>
	/// Connection point of curve's end node in relative curve.
	/// </summary>
	public BezierConnectionPoint endConnectionPoint = BezierConnectionPoint.Start;

	/// <summary>
	/// Relative curve for connection curve's end node.
	/// </summary>
	public BezierCurve endConnectedCurve = null;

	/// <summary>
	/// Rotation modifier for curve.
	/// </summary>
	public AnimationCurve rotationCurve = AnimationCurve.Linear(0,0,1,0);

	/// <summary>
	/// Flag of ignore inflection points in up vector calculation.
	/// </summary>
	//public bool ignoreInflectionPoints = false;
	#endregion

	#region Methods
	/// <summary>
	/// Get acceleration in selected point of curve. Calculate directly from local curve object.
	/// </summary>
	/// <param name="t">Position on curve [0,1]</param>
	/// <returns>Acceleration vector in global transform.</returns>
	public override Vector3 GetAcceleration(float t)
	{
		if(autoUpdate || !Application.isPlaying)
			VerifyCurve ();

		t = Mathf.Clamp01(t);
		return transform.TransformPoint (curve.GetSecondDerivative (t))-transform.position;
	}

	/// <summary>
	/// Get local curve object.
	/// </summary>
	/// <returns>Curve data object.</returns>
	public Bezier GetCurve()
	{
		if(autoUpdate || !Application.isPlaying)
			VerifyCurve ();

		return curve;
	}

	/// <summary>
	/// Get curve end node.
	/// </summary>
	/// <returns>End node of curve.</returns>
	public override BezierNode GetEndNode()
	{
		return GetCurve().endNode;
	}

	/// <summary>
	/// Get forward direction in selected point of curve. Calculate directly from local curve object.
	/// </summary>
	/// <param name="t">Position on curve [0,1]</param>
	/// <returns>Forward direction vector in global transform.</returns>
	public override Vector3 GetForward(float t)
	{
		t = Mathf.Clamp01(t);
		return GetVelocity (t).normalized;
	}

	/// <summary>
	/// Get distance length from start to selected point on curve. Calculate directly from local curve object.
	/// Arc length is calculate with Legendre-Gauss quadrature. It is approximation method, but gets good accuracy with great efficiency.
	/// </summary>
	/// <param name="t">Position on curve [0,1]</param>
	/// <returns>Length of curve element.</returns>
	public override float GetLength (float t)
	{
		//First point of approximation.
		float z = t *( (-1f + Mathf.Sqrt (3f)) / (2f * Mathf.Sqrt (3f))); 
		Vector3 Point = transform.TransformVector(curve.GetFirstDerivative (z));
		//Value of length function in first approximation point.
		float f = Mathf.Sqrt (Point.x * Point.x + Point.y * Point.y + Point.z * Point.z);
		//Second point of approximation.
		z = t *( (1f + Mathf.Sqrt (3f)) / (2f * Mathf.Sqrt (3f)));
		Point = transform.TransformVector(curve.GetFirstDerivative (z));
		//Sum values of length function in both approximation point.
		f += Mathf.Sqrt (Point.x * Point.x + Point.y * Point.y + Point.z * Point.z);
		f *= t / 2f;
		return f;
	}

	/// <summary>
	/// Get position in selected point of curve. Calculate directly from local curve object.
	/// </summary>
	/// <param name="t">Position on curve [0,1]</param>
	/// <returns>Point coordinates in global transform.</returns>
	public override Vector3 GetPosition(float t)
	{
		if(autoUpdate || !Application.isPlaying)
			VerifyCurve ();
		
		t = Mathf.Clamp01(t);
		return transform.TransformPoint(curve.GetPoint(t));
	}

	/// <summary>
	/// Get rotation around curve in selected point. Calculate directly from local rotation.
	/// </summary>
	/// <param name="t">Position on curve [0,1]</param>
	/// <returns>Rotation in angles.</returns>
	public override float GetRotation(float t)
	{
		if(autoUpdate || !Application.isPlaying)
			VerifyCurve ();
		
		t = Mathf.Clamp01(t);
		return rotationCurve.Evaluate(t);
	}

	/// <summary>
	/// Get curve start node.
	/// </summary>
	/// <returns>Start node of curve.</returns>
	public override BezierNode GetStartNode()
	{
		return GetCurve().startNode;
	}

	/// <summary>
	/// Get up direction in selected point of curve. Calculate directly from local curve object.
	/// </summary>
	/// <param name="t">Position on curve [0,1]</param>
	/// <param name="ignoreInflectionPoints">Ignore inflection points of curve. Improve rotation continuity.</param>
	/// <param name="ignoreRotation">Ignore manual rotation changes. Get clear up direction vector.</param>
	/// <returns>Up direction vector in global transform.</returns>
	public override Vector3 GetUp(float t, bool includeInflectionPoints = true, bool ignoreRotation = false)
	{

		t = Mathf.Clamp01(t);
		float rotation = GetRotation(t);
		Vector3 forward = GetForward (t);
		//Calculate normal vector for plane created from velocity and acceleration vectors.
		Vector3 up = Vector3.Cross (forward, -GetAcceleration (t).normalized);

		//Update direction with inflection points.
		if(includeInflectionPoints)
			for (int i = 0; i < 3; i++)
				up = GetUpInflectionVerify (t, up, (BezierPlane)i);

		//Inclusion of manual rotation changes.
		if (ignoreRotation)
			return up.normalized;
		else
			return Quaternion.AngleAxis(rotation,forward)*up.normalized;
	}

	/// <summary>
	/// Update up direction vector with inflection points for selected plane.
	/// </summary>
	/// <param name="t">Position on curve [0,1]</param>
	/// <param name="up">Up direction vector.</param>
	/// <param name="plane">Selected plane for verification.</param>
	/// <returns>Updated up direction vector.</returns>
	Vector3 GetUpInflectionVerify(float t, Vector3 up , BezierPlane plane)
	{
		float[] temp = curve.InflectionPoints (plane);
		Vector3 scale = Vector3.one;

		switch (plane) 
		{
		case BezierPlane.XY:
			{
				scale.z = -1f;
				break;
			}
		case BezierPlane.YZ:
			{
				scale.x = -1f;
				break;
			}
		case BezierPlane.ZX:
			{
				scale.y = -1f;
				break;
			}
		}
		if (temp.Length == 1 && t > temp [0]) 
		{
			up.Scale (scale);
		}
		if (temp.Length == 2 && t > temp [0] && t < temp[1]) 
		{
			up.Scale (scale);
		}
		return up;
	}

	/// <summary>
	/// Get velocity in selected point of curve. Calculate directly from local curve object.
	/// </summary>
	/// <param name="t">Position on curve [0,1]</param>
	/// <returns>Velocity vector in global transform.</returns>
	public override Vector3 GetVelocity(float t)
	{
		if(autoUpdate || !Application.isPlaying)
			VerifyCurve ();
		
		t = Mathf.Clamp01(t);
		return transform.TransformPoint (curve.GetFirstDerivative (t))-transform.position;
	}

	public override List<float> IntersectionLine (Vector3 position, Vector3 direction,float accuracy = 0.01f, float floatAccuracy = 0.00001f)
	{
		if(autoUpdate || !Application.isPlaying)
			VerifyCurve ();

		return curve.IntersectionLine (transform.InverseTransformPoint (position), transform.InverseTransformDirection (direction),accuracy,floatAccuracy);	
	}

	public override List<float> IntersectionPlane (Vector3 position, Vector3 normal, float floatAccuracy = 0.00001f)
	{
		if(autoUpdate || !Application.isPlaying)
			VerifyCurve ();
		
		return curve.IntersectionPlane (transform.InverseTransformPoint (position), transform.InverseTransformDirection (normal),floatAccuracy);
	}

	/// <summary>
	/// Change direction of curve in component. Calculate directly from local curve object.
	/// With change direction of curve object, it is changed points of connected relative curves and previous/next curves lists.
	/// </summary>
	public override void Reverse()
	{
		if(autoUpdate || !Application.isPlaying)
			VerifyCurve ();
		
		curve.Reverse ();

		List<BezierCurve> tempList = nextCurves;
		BezierCurve tempCurve = startConnectedCurve;
		BezierConnectionType tempConnectionType = startConnectionType;
		BezierConnectionPoint tempConnectionPoint = startConnectionPoint;


		nextCurves = previousCurves;
		previousCurves = tempList;
		startConnectedCurve = endConnectedCurve;
		startConnectionPoint = endConnectionPoint;
		startConnectionType = endConnectionType;
		endConnectedCurve = tempCurve;
		endConnectionType = tempConnectionType;
		endConnectionPoint = tempConnectionPoint;
	}

	/// <summary>
	/// Set local curve object.
	/// </summary>
	/// <param name="curve">New curve data object.</param>
	public virtual void SetCurve(Bezier curve)
	{
		this.curve = curve;
	}

	/// <summary>
	/// Verify relative curves on start.
	/// </summary>
	void Start()
	{
		VerifyCurve ();
	}

	/// <summary>
	/// Update curve, if relative curves are changed.
	/// </summary>
	public virtual void VerifyCurve()
	{
		if (startConnectedCurve != null && (int)startConnectionType != 0) 
			VerifyNode (startConnectionType,startConnectionPoint, curve.startNode, startConnectedCurve);
		if (endConnectedCurve != null && (int)endConnectionType != 0) 
			VerifyNode (endConnectionType,endConnectionPoint, curve.endNode, endConnectedCurve);
	}

	/// <summary>
	/// Update node, if relative curve are changed.
	/// </summary>
	/// <param name="type">Type of connection for verified node.</param>
	/// <param name="point">Connection point of relative curve for verified node.</param>
	/// <param name="node">Node for verify.</param>
	/// <param name="connectedCurve">Relative curve for verified node.</param>
	void VerifyNode(BezierConnectionType type,BezierConnectionPoint point,BezierNode node,BezierCurve connectedCurve)
	{
		int tempType = (int)type;

		if (tempType == 0)
			return;

		BezierNode connectedNode;
		if (point == BezierConnectionPoint.Start)
			connectedNode = connectedCurve.GetStartNode().TransformNode(connectedCurve.transform,transform);
		else 
			connectedNode = connectedCurve.GetEndNode().TransformNode(connectedCurve.transform,transform);

		node.mainPosition = connectedNode.mainPosition;

		if (tempType >= 2)
			node.rotation = connectedNode.rotation;
		if (tempType == 3 || tempType == 5)
			node.rotation += new Vector3 (180,0, 0); 
		if (tempType == 4 || tempType == 5)
			node.length = connectedNode.length;
	}
	#endregion

	#region EditorOnly
	#if UNITY_EDITOR
	void OnDrawGizmos()
	{
		for (float i = 0; i < resolution; i++) 
		{	
			Gizmos.color = gizmoColor;
			Gizmos.DrawLine (GetPosition (i/((float)resolution)), GetPosition ((i+1f)/((float)resolution)));
		}
	}
	public bool showStartNode = true;
	public bool showEndNode = true;
	#endif
	#endregion
}
