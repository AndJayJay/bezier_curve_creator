﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Orientation of plane in local transform 
/// </summary>
public enum BezierPlane {
	///<summary> plane between axis X and axis Y</summary>
	XY,
	///<summary> plane between axis Y and axis Z</summary>
	YZ,
	///<summary> plane between axis Z and axis X</summary>
	ZX
}; 

/// <summary>
/// Type of connection between two BezierNodes
/// </summary>
public enum BezierConnectionType {
	///<summary> No connection </summary>
	None,
	///<summary> BezierNode copy mainPosition from relative BezierNode</summary>
	Connected,
	///<summary> BezierNode copy mainPosition and rotation from relative BezierNode</summary>
	Align,
	///<summary> BezierNode copy mainPosition and negative rotation from relative BezierNode</summary>
	AlignReverse,
	///<summary> BezierNode copy mainPosition, rotation and length from relative BezierNode</summary>
	Mirror,
	///<summary> BezierNode copy mainPosition, negative rotation and length from relative BezierNode</summary>
	MirrorReverse
};

/// <summary>
/// Point of connection to relative BezierCurve
/// </summary>
public enum BezierConnectionPoint {
	///<summary> Connection with StartNode of relative BezierCurve</summary>
	Start=1,
	///<summary> Connection with EndNode of relative BezierCurve</summary>
	End=-1
};

/// <summary>
/// Transform of calculate data from relative BezierCurve
/// </summary>
public enum BezierSpace {
	///<summary> Get data from transform of relative BezierCurve</summary>
	Local,
	///<summary> Get data from global transform</summary>
	World,
	///<summary> Get data from own transform</summary>
	Self
};



/// <summary>
/// Class for mathematic calculating of quadratic bezier curve
/// </summary>
[System.Serializable]
public class Bezier 
{
	#region Attributes
	/// <summary>
	///Start node of curve. Define position and direction of start point of curve.
	/// </summary>
	public BezierNode startNode = new BezierNode (new Vector3(-1,0,0));

	/// <summary>
	///End node of curve. Define position and direction of end point of curve.
	/// </summary>
	public BezierNode endNode = new BezierNode (new Vector3 (1f, 0, 0));
	#endregion

	#region Constructors
	/// <summary>
	///Empty constructor. Create default curve.
	/// </summary>
	public Bezier(){}

	/// <summary>
	///Constructor. Define curve with nodes.
	/// </summary>
	/// <param name="startNode"> StartNode for curve</param>
	/// <param name="endNode"> EndNode for curve</param>
	public Bezier(BezierNode startNode, BezierNode endNode)
	{
		this.startNode = startNode;
		this.endNode = endNode;
	}

	/// <summary>
	///Constructor. Define curve with four points.
	/// </summary>
	///<param name="p0"> Start point of curve</param>
	///<param name="p1"> First reference point of curve</param>
	///<param name="p2"> Second reference point of curve</param>
	///<param name="p3"> End point of curve</param>
	public Bezier(Vector3 p0,Vector3 p1, Vector3 p2, Vector3 p3)
	{
		this.startNode = new BezierNode (p0, p1);
		this.endNode = new BezierNode (p3, p2);
	}
	#endregion

	#region Methods
	public void Align(Vector3 direction)
	{
		float rotation = Vector3.Angle (direction,startNode.mainPosition-endNode.mainPosition);
		if (rotation != 0) 
		{
			Vector3 axis = Vector3.Cross (startNode.mainPosition-endNode.mainPosition, direction).normalized;
			Rotate (rotation, axis);
		}
	}

	/// <summary>
	///Compare with second curve.
	/// </summary>
	///<param name="curve"> Second curve</param>
	/// <returns>True if both curves have the same values of nodes</returns>
	public  bool Compare (Bezier curve)
	{
		if (startNode.Compare(curve.startNode) && endNode.Compare(curve.endNode)) 
		{
			return true;
		} 
		else 
		{
			return false;
		}
	}

	/// <summary>
	///Create copy of curve.
	/// </summary>
	/// <returns>Copy of curve</returns>
	public Bezier Copy()
	{
		return new Bezier (startNode.Copy (), endNode.Copy ());
	}

	/// <summary>
	///Get value of curve's first derative.
	/// </summary>
	///<param name="t">Position on curve [0,1]</param>
	/// <returns>Value of curve's first derative in t parameter position</returns>
	public Vector3 GetFirstDerivative(float t)
	{
		Vector3 g,h,i;
		g = -3f * startNode.mainPosition + 9f * startNode.referencePosition - 9f * endNode.referencePosition + 3f * endNode.mainPosition;
		h = 6f * startNode.mainPosition - 12f * startNode.referencePosition + 6f * endNode.referencePosition;
		i = -3f * startNode.mainPosition + 3f * startNode.referencePosition;
		return t * t * g + t * h + i;
	}	

	/// <summary>
	///Get length of curve, from start to selected point.
	/// </summary>
	///<param name="t">Position on curve [0,1]</param>
	/// <returns>Length of curve, from start to position in t parameter</returns>
	public float GetLength (float t)
	{
		t = Mathf.Clamp01 (t);

		float z = t *( (-1f + Mathf.Sqrt (3f)) / (2f * Mathf.Sqrt (3f))); 
		Vector3 Point = GetFirstDerivative (z);
		float f = Mathf.Sqrt (Point.x * Point.x + Point.y * Point.y + Point.z * Point.z);
		z = t *( (1f + Mathf.Sqrt (3f)) / (2f * Mathf.Sqrt (3f)));
		Point = GetFirstDerivative (z);
		f += Mathf.Sqrt (Point.x * Point.x + Point.y * Point.y + Point.z * Point.z);
		f *= t / 2f;

		return f;
	}

	/// <summary>
	///Get position of point on curve.
	/// </summary>
	///<param name="t">Position on curve [0,1]</param>
	/// <returns>Coordinates of point on curve in t parameter position</returns>
	public Vector3 GetPoint(float t)
	{
		Vector3 a,b,c,d;
		a = -startNode.mainPosition + 3f * startNode.referencePosition - 3f * endNode.referencePosition + endNode.mainPosition;
		b = 3f * startNode.mainPosition - 6f * startNode.referencePosition + 3f * endNode.referencePosition;
		c = -3f * startNode.mainPosition + 3f * startNode.referencePosition;
		d = startNode.mainPosition;
		return t * t * t * a + t * t * b + t * c + d;

	}

	/// <summary>
	///Get value of curve's second derative.
	/// </summary>
	///<param name="t">Position on curve [0,1]</param>
	/// <returns>Value of curve's second derative in t parameter position</returns>
	public Vector3 GetSecondDerivative(float t)
	{
		return 6f * t*(endNode.mainPosition + 3f * (startNode.referencePosition - endNode.referencePosition) - startNode.mainPosition) + 6f * (startNode.mainPosition - 2f * startNode.referencePosition + endNode.referencePosition);
	}

	/// <summary>
	///Calculate inflection points of curve in selected plane.
	/// </summary>
	///<param name="plane">Selected plane.</param>
	/// <returns>Array of inflection points as values of t parameter on curve.</returns>
	public float[] InflectionPoints(BezierPlane plane)
	{
		Vector3 tempStartRef, tempEndRef, tempEndMain;
		tempStartRef = startNode.referencePosition - startNode.mainPosition;
		tempEndRef = endNode.referencePosition - startNode.mainPosition;
		tempEndMain = endNode.mainPosition - startNode.mainPosition;
		switch (plane) 
		{
		case BezierPlane.XY:
			{
				return InflectionPointsPlane(new Vector2(tempStartRef.x,tempStartRef.y),new Vector2(tempEndRef.x,tempEndRef.y),new Vector2(tempEndMain.x,tempEndMain.y));
			}
		case BezierPlane.YZ:
			{
				return InflectionPointsPlane(new Vector2(tempStartRef.y,tempStartRef.z),new Vector2(tempEndRef.y,tempEndRef.z),new Vector2(tempEndMain.y,tempEndMain.z));
			}
		case BezierPlane.ZX:
			{
				return InflectionPointsPlane(new Vector2(tempStartRef.z,tempStartRef.x),new Vector2(tempEndRef.z,tempEndRef.x),new Vector2(tempEndMain.z,tempEndMain.x));
			}
		}
		return new float[2];
	}

	/// <summary>
	///Calculate inflection points of curve in plane.
	/// </summary>
	///<param name="tempStartRef">First reference point on simplified plane.</param>
	///<param name="tempEndRef">Second reference point on simplified plane.</param>
	///<param name="tempEndMain">End point on simplified plane.</param>
	/// <returns>Array of inflection points as values of t parameter on curve.</returns>
	private float [] InflectionPointsPlane(Vector2 tempStartRef,Vector2 tempEndRef,Vector2 tempEndMain)
	{
		float a, b, c, d,e,f;
		a = tempEndRef.x * tempStartRef.y;
		b = tempEndMain.x * tempStartRef.y;
		c = tempStartRef.x * tempEndRef.y;
		d = tempEndMain.x * tempEndRef.y;
		e = tempStartRef.x * tempEndMain.y;
		f = tempEndRef.x * tempEndMain.y;

		float x = (-3f*a+2f*b+3f*c-d-2f*e+f);
		float y = (3f*a-b-3f*c+e);
		float z = (c - a);

		List<float> roots;

		roots = RootsQuadratic (x, y, z);
		roots.RemoveAll(var =>( var<0f || var>1f));
		roots.Sort ();

		return roots.ToArray();
	}


	public List<float> IntersectionLine (Vector3 position, Vector3 direction,float accuracy = 0.01f, float floatAccuracy = 0.00001f)
	{
		var curve = Copy ();
		curve.Translate (-position);

		float rotation = Vector3.Angle (direction,Vector3.right);
		if (rotation != 0) 
		{
			Vector3 axis = Vector3.Cross (direction,Vector3.right).normalized;
			curve.Rotate (rotation, axis);
		}

		Vector3 a,b,c,d;
		a = -curve.startNode.mainPosition + 3f * curve.startNode.referencePosition - 3f * curve.endNode.referencePosition + curve.endNode.mainPosition;
		b = (3f * curve.startNode.mainPosition - 6f * curve.startNode.referencePosition + 3f * curve.endNode.referencePosition);
		c = (-3f * curve.startNode.mainPosition + 3f * curve.startNode.referencePosition);
		d = curve.startNode.mainPosition;

		if (Mathf.Abs (a.z) < floatAccuracy)
			a.z = 0;
		if (Mathf.Abs (b.z) < floatAccuracy)
			b.z = 0;
		if (Mathf.Abs (c.z) < floatAccuracy)
			c.z = 0;
		if (Mathf.Abs (d.z) < floatAccuracy)
			d.z = 0;


		if (Mathf.Abs (a.y) < floatAccuracy)
			a.y = 0;
		if (Mathf.Abs (b.y) < floatAccuracy)
			b.y = 0;
		if (Mathf.Abs (c.y) < floatAccuracy)
			c.y = 0;
		if (Mathf.Abs (d.y) < floatAccuracy)
			d.y = 0;
		
		var rootsZ = RootsCubic(a.z,b.z,c.z,d.z);
		if (rootsZ.Count == 0 && d.z < accuracy)
			rootsZ.Add (float.PositiveInfinity);

		var rootsY = RootsCubic(a.y,b.y,c.y,d.y);
		if (rootsY.Count == 0 && d.y < accuracy)
			rootsY.Add (float.PositiveInfinity);

		List<float> roots = new List<float> ();
		accuracy = Mathf.Clamp01 (accuracy);

		foreach (var item in rootsZ) 
		{			
			if (rootsY.Exists (x => (Mathf.Abs (x - item) < accuracy)||(float.IsPositiveInfinity(x))))
				roots.Add (item);
			else if (float.IsPositiveInfinity (item))
				roots = rootsY;
		}

		roots.RemoveAll(var =>( (var<0f || var>1f)&& !float.IsPositiveInfinity(var)));
		roots.Sort ();
		return roots;
	}

	public List<float> IntersectionPlane (Vector3 position, Vector3 normal, float floatAccuracy = 0.00001f)
	{
		var curve = Copy ();
		curve.Translate (-position);

		float rotation = Vector3.Angle (normal,Vector3.right);
		if (rotation != 0) 
		{
			Vector3 axis = Vector3.Cross (normal,Vector3.right).normalized;
			curve.Rotate (rotation, axis);
		}

		float a,b,c,d;
		a = -curve.startNode.mainPosition.x + 3f * curve.startNode.referencePosition.x - 3f * curve.endNode.referencePosition.x + curve.endNode.mainPosition.x;
		if (Mathf.Abs (a) < floatAccuracy)
			a = 0;
		b = (3f * curve.startNode.mainPosition.x - 6f * curve.startNode.referencePosition.x + 3f * curve.endNode.referencePosition.x);
		if (Mathf.Abs (b) < floatAccuracy)
			b = 0;
		c = (-3f * curve.startNode.mainPosition.x + 3f * curve.startNode.referencePosition.x);
		if (Mathf.Abs (c) < floatAccuracy)
			c = 0;
		d = curve.startNode.mainPosition.x;
		if (Mathf.Abs (d) < floatAccuracy)
			d = 0;
		
		var roots = RootsCubic(a,b,c,d);
		roots.RemoveAll(var =>( (var<0f || var>1f)&& !float.IsPositiveInfinity(var)));
		roots.Sort ();
		return roots;
	}

	/// <summary>
	///Change transform of curve from global transform to selected transform.
	/// </summary>
	/// <param name="endTransform">Destiny transform.</param>
	public Bezier InverseTransformBezier(Transform endTransform)
	{
		return new Bezier(startNode.InverseTransformNode(endTransform),endNode.InverseTransformNode(endTransform));
	}

	/// <summary>
	///Change direction of curve.
	/// </summary>
	public void Reverse()
	{
		BezierNode tempNode = startNode;
		startNode = endNode;
		endNode = tempNode;
	}

	/// <summary>
	///Change direction of curve.
	/// </summary>
	/// <param name="curve">Curve to reverse.</param>
	public static void Reverse(Bezier curve)
	{
		BezierNode tempNode = curve.startNode;
		curve.startNode = curve.endNode;
		curve.endNode = tempNode;
	}

	List<float> RootsLinear(float a,float b)
	{
		if (a == 0 && b == 0)
			return new List<float>(){float.PositiveInfinity};		
		else if(a==0 && b!= 0)
			return new List<float>();
		else
			return new List<float>(){ -b / a};
	}

	List<float> RootsQuadratic(float a,float b,float c)
	{
		if (a == 0f)
			return RootsLinear(b,c);

		float delta = b * b - 4f * a * c;
		List<float> roots = new List<float>();

		if (delta == 0)
			roots.Add(-b / (2f * a));
		if (delta > 0) 
		{
			roots.Add((-b + Mathf.Sqrt (delta)) / (2f * a));
			roots.Add((-b - Mathf.Sqrt (delta)) / (2f * a));
		}
		return roots;
	}

	List<float> RootsCubic(float a,float b,float c,float d)
	{
		if (a == 0f) 
		{
			return RootsQuadratic (b, c, d);
		}

		float p, p3, q, q2, delta;
		List<float> roots = new List<float>();

		b /= a;
		c /= a;
		d /= a;

		p = (3f * c - b*b) / 3f;
		p3 = p / 3f;

		q = (2f * b*b*b - 9f * b*c + 27f * d) / 27f;
		q2 = q / 2f;

		delta = q2*q2 + p3*p3*p3;

		if (delta < 0) 
		{
			float i = Mathf.Sqrt (q2 * q2 - delta);
			float j = Mathf.Sign(i)*Mathf.Pow (Mathf.Abs(i), 1f / 3f);
			float k = Mathf.Acos (-q2 / i);
			float m = Mathf.Cos (k / 3f);
			float n = Mathf.Sqrt (3f) * Mathf.Sin (k / 3f);

			roots.Add(2f * j * m - b / 3f);
			roots.Add(-j * (m + n) - b / 3f);
			roots.Add( -j * (m - n) - b / 3f);
		} 
		else if (delta > 0) 
		{
			float sd = Mathf.Sqrt (delta);
			float u1 = sd - q2;
			u1 = Mathf.Sign(u1)*Mathf.Pow (Mathf.Abs(u1), 1f / 3f);
			float v1 = sd + q2;
			v1 = Mathf.Sign(v1)*Mathf.Pow (Mathf.Abs(v1), 1f / 3f);
			roots.Add( u1 - v1 - b / 3f);
		} 
		else if (p == 0 && q == 0) 
		{
			roots.Add(-Mathf.Sign(d)*Mathf.Pow (Mathf.Abs(d), 1f / 3f));
		} 
		else if (delta == 0) 
		{
			float u1 = -Mathf.Sign(q2)*Mathf.Pow (Mathf.Abs(q2), 1f / 3f);
			roots.Add (2f * u1 - b / 3f);
			roots.Add (-u1 - b / 3f);
		}
		return roots;
	}

	/// <summary>
	///Rotates curve, angle deegres around axis.
	/// </summary>
	/// <param name="angle">Angle in deegres.</param>
	/// <param name="axis">Rotation axis.</param>
	public void Rotate(float angle, Vector3 axis)
	{
		startNode.Rotate (angle, axis);
		endNode.Rotate (angle, axis);
	}

	/// <summary>
	///Split bezier curve to two independent curves. 
	/// </summary>
	/// <param name="t">Point of split [0,1].</param>
	/// <returns>Array of curves, which represent fragments of original curve.</returns>
	public Bezier[] Split(float t)
	{
		t = Mathf.Clamp01 (t);
		if (t == 0f || t == 1f) 
		{
			Bezier curve = new Bezier(startNode.Copy (),endNode.Copy());
			return new[]{curve};
		}

		Vector3 a = Vector3.Lerp(startNode.mainPosition, startNode.referencePosition, t);
		Vector3 b = Vector3.Lerp(startNode.referencePosition, endNode.referencePosition, t);
		Vector3 c = Vector3.Lerp(endNode.referencePosition, endNode.mainPosition, t);
		Vector3 m = Vector3.Lerp(a, b, t);
		Vector3 n = Vector3.Lerp(b, c, t);
		Vector3 p = GetPoint (t);

		Bezier[] newCurves = new Bezier[2];

		newCurves [0] = new Bezier (startNode.mainPosition, a, m, p);
		newCurves [1] = new Bezier (p, n, c, endNode.mainPosition);

		return newCurves;
	}

	/// <summary>
	///Points, which define bezier quadratic curve.
	/// </summary>
	/// <param name="index">Index of point [0,3]</param>
	/// <returns>Point of bezier curve</returns>
	public Vector3 this[int index]
	{
		get 
		{
			if (index < 0 || index > 3)
				Debug.LogWarning ("Bezier iterator overloaded!");

			index = Mathf.Clamp (index, 0, 3);
			Vector3 outVector = Vector3.zero;
			switch (index) {
			case 0:
				{
					outVector = startNode.mainPosition;
					break;
				}
			case 1:
				{
					outVector = startNode.referencePosition;
					break;
				}
			case 2:
				{
					outVector = endNode.referencePosition;
					break;
				}
			case 3:
				{
					outVector = endNode.mainPosition;
					break;
				}
			}
			return outVector;
		}
		set 
		{
			if (index < 0 || index > 3)
				Debug.LogWarning ("Bezier iterator overloaded!");

			index = Mathf.Clamp (index, 0, 3);
			switch (index) {
			case 0:
				{
					startNode.mainPosition = value;
					break;
				}
			case 1:
				{
					startNode.referencePosition = value;
					break;
				}
			case 2:
				{
					endNode.referencePosition = value;
					break;
				}
			case 3:
				{
					endNode.mainPosition = value;
					break;
				}
			}
		}
	}

	/// <summary>
	///Simple information about curve in string format.
	/// </summary>
	/// <returns>Curve data.</returns>
	public override string ToString ()
	{
		return string.Format ("Start Pos:{0}, End Pos:{1}", startNode.mainPosition,endNode.mainPosition);
	}

	/// <summary>
	///Change transform of curve from selected transform to global transform.
	/// </summary>
	/// <param name="startTransform">Transform of curve.</param>
	public Bezier TransformBezier(Transform startTransform)
	{		
		return new Bezier(startNode.TransformNode(startTransform),endNode.TransformNode(startTransform));
	}

	/// <summary>
	///Change transform of curve from first transform to second transform.
	/// </summary>
	/// <param name="startTransform">Transform of curve.</param>
	/// <param name="endTransform">Destiny transform.</param>
	public Bezier TransformBezier(Transform startTransform,Transform endTransform)
	{
		return new Bezier(startNode.TransformNode(startTransform,endTransform),endNode.TransformNode(startTransform,endTransform));
	}

	public void Translate(Vector3 translation)
	{
		startNode.mainPosition += translation;
		endNode.mainPosition += translation;
	}

	#endregion
}

/// <summary>
/// Class contains data for single bezier node. Each curve have two nodes: start and end.
/// </summary>
[System.Serializable]
public class BezierNode
{
	#region Attributes
	/// <summary>
	/// Main position of node in local transform.
	/// </summary>
	public Vector3 mainPosition = Vector3.zero;

	/// <summary>
	/// Euler rotation of reference position around main position in local transform.
	/// </summary>
	public Vector3 rotation = Vector3.zero;

	/// <summary>
	/// Distance between main position and reference position.
	/// </summary>
	public float length = 1f;
	#endregion

	#region Properties
	/// <summary>
	/// Reference position of node. Calculated from rotation and length.
	/// </summary>
	public Vector3 referencePosition
	{
		get
		{
			return mainPosition + Quaternion.Euler (rotation)*Vector3.forward * length; 
		}
		set
		{
			if(value != referencePosition)
			{
				Vector3 temp = value - mainPosition;
				length = temp.magnitude;
				rotation = Quaternion.LookRotation (temp).eulerAngles;
			}
		}
	}
	#endregion

	#region Constructors
	public BezierNode()
	{
	}

	public BezierNode(Vector3 mainPos)
	{
		mainPosition = mainPos;
	}

	public BezierNode(Vector3 mainPos, Vector3 referencePos)
	{
		mainPosition = mainPos;
		referencePosition = referencePos;
	}

	public BezierNode(Vector3 mainPos, Vector3 rotation, float length)
	{
		mainPosition = mainPos;
		this.rotation = rotation;
		this.length = length;
	}
	#endregion

	#region Methods
	/// <summary>
	///Compare with second node.
	/// </summary>
	///<param name="node"> Second node</param>
	/// <returns>True if both node have the same main position and reference position values.</returns>
	public  bool Compare (BezierNode node)
	{
		if (node != null && mainPosition == node.mainPosition && referencePosition == node.referencePosition) 
		{
			return true;
		} 
		else 
		{
			return false;
		}
	}

	/// <summary>
	///Create copy of node.
	/// </summary>
	/// <returns>Copy of node</returns>
	public  BezierNode Copy ()
	{
		return new BezierNode (mainPosition, referencePosition);
	}

	/// <summary>
	///Calculate direction vector from main position to reference position. 
	/// </summary>
	/// <returns>Node direction in local transform.</returns>
	public Vector3 GetDirection()
	{
		return Quaternion.Euler(rotation)*Vector3.forward;
	}

	/// <summary>
	///Calculate vector from main position to reference position. 
	/// </summary>
	/// <returns>Node vector in local transform.</returns>
	public Vector3 GetVector()
	{
		return Quaternion.Euler(rotation)*Vector3.forward*length;
	}

	/// <summary>
	///Change transform of node from global transform to selected transform.
	/// </summary>
	/// <param name="endTransform">Destiny transform.</param>
	public BezierNode InverseTransformNode(Transform endTransform)
	{
		Vector3 mainPos = endTransform.InverseTransformPoint (mainPosition);
		Vector3 referencePos = endTransform.InverseTransformPoint (referencePosition);

		return new BezierNode (mainPos,referencePos);
	}

	/// <summary>
	///Linearly interpolates between two nodes.
	/// </summary>
	/// <param name="a">Start node.</param>
	/// <param name="b">End node.</param>
	/// <param name="t">Point of interpolation [0,1]</param>
	/// <returns>Interpolated node.</returns>
	public static BezierNode Lerp(BezierNode a,BezierNode b,float t)
	{
		BezierNode node = new BezierNode ();
		node.mainPosition = Vector3.Lerp (a.mainPosition, b.mainPosition, t);
		node.referencePosition = Vector3.Lerp (a.referencePosition, b.referencePosition, t);
		return node;
	}

	/// <summary>
	///Rotates node, angle deegres around axis.
	/// </summary>
	/// <param name="angle">Angle in deegres.</param>
	/// <param name="axis">Rotation axis.</param>
	public void Rotate(float angle, Vector3 axis)
	{
		Vector3 temp = referencePosition;

		mainPosition = Quaternion.AngleAxis (angle, axis)*mainPosition;
		referencePosition = Quaternion.AngleAxis (angle, axis)*temp;
	}

	/// <summary>
	///Change direction of node.
	/// </summary>
	/// <param name="direction">New direction of node in local transform.</param>
	public void SetDirection(Vector3 direction)
	{
		rotation = Quaternion.LookRotation (direction).eulerAngles;
	}

	/// <summary>
	///Spherically interpolates between two nodes.
	/// </summary>
	/// <param name="a">Start node.</param>
	/// <param name="b">End node.</param>
	/// <param name="t">Point of interpolation [
	public static BezierNode Slerp(BezierNode a,BezierNode b,float t)
	{
		BezierNode node = new BezierNode ();
		node.mainPosition = Vector3.Slerp (a.mainPosition, b.mainPosition, t);
		node.referencePosition = Vector3.Slerp (a.referencePosition, b.referencePosition, t);
		return node;
	}

	/// <summary>
	///Simple information about node in string format.
	/// </summary>
	/// <returns>Node data.</returns>
	public override string ToString ()
	{
		return string.Format ("MainPosition: {0}, Length: {1}, Rotation: {2}", mainPosition,length,rotation);
	}

	/// <summary>
	///Change transform of node from selected transform to global transform.
	/// </summary>
	/// <param name="startTransform">Transform of node.</param>
	public BezierNode TransformNode(Transform startTransform)
	{
		Vector3 mainPos = startTransform.TransformPoint (mainPosition);
		Vector3 referencePos = startTransform.TransformPoint (referencePosition);

		return new BezierNode (mainPos,referencePos);
	}

	/// <summary>
	///Change transform of node from first transform to second transform.
	/// </summary>
	/// <param name="startTransform">Transform of node.</param>
	/// <param name="endTransform">Destiny transform.</param>
	public BezierNode TransformNode(Transform startTransform,Transform endTransform)
	{
		Vector3 mainPos = endTransform.InverseTransformPoint(startTransform.TransformPoint (mainPosition));
		Vector3 referencePos = endTransform.InverseTransformPoint(startTransform.TransformPoint (referencePosition));

		return new BezierNode (mainPos,referencePos);
	}
	#endregion
}

/// <summary>
/// Class represent single trigger on curve.
/// </summary>
[System.Serializable]
public class BezierTrigger
{
	#region Attributes
	/// <summary>
	/// Name of trigger.
	/// </summary>
	public string name = "";
	#endregion

	#region Properties
	[SerializeField]
	[Range(0f,1f)]
	float _start = 0f;
	/// <summary>
	/// Value of start point of activation trigger on curve [0,1].
	/// </summary>
	public float start
	{
		get 
		{
			return _start;	
		}
		set 
		{
			if (value != _start) 
			{
				if (value > end)
					_start = end;
				else 
				{
					if (value > 1f)
						_start = 1f;
					else if (value < 0f)
						_start = 0f;
					else
						_start = value;
				}
			}
		}
	}

	[SerializeField]
	[Range(0f,1f)]
	float _end = 1f;
	/// <summary>
	/// Value of end point of activation trigger on curve [0,1].
	/// </summary>
	public float end
	{
		get 
		{
			return _end;	
		}
		set 
		{
			if (value != _end) 
			{
				if (value < start)
					_end = start;
				else 
				{
					if (value > 1f)
						_end = 1f;
					else if (value < 0f)
						_end = 0f;
					else
						_end = value;
				}
			}
		}
	}
	#endregion

	#region Constructors
	/// <summary>
	/// Default constructor. In default trigger start at 0 and end in 1.
	/// </summary>
	public BezierTrigger(){}

	/// <summary>
	/// Constructor.
	/// </summary>
	/// <param name="start">Start of trigger activation</param>
	/// <param name="end">End of trigger activation</param>
	public BezierTrigger(float start,float end)
	{
		this.start = start;
		this.end = end;
	}
	#endregion

	#region Methods
	/// <summary>
	/// Check activity of trigger.
	/// </summary>
	///<returns>True, if trigger is active in selected point
	public bool Check(float t)
	{
		if (t >= start && t <= end)
			return true;
		else
			return false;
	}
	#endregion
}