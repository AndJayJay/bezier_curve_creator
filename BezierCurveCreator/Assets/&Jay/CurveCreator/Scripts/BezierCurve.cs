﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Abstract class for quadratic bezier curve component in inspector.
/// </summary>
public abstract class BezierCurve: MonoBehaviour
{
	#region Attributes
	/// <summary>
	/// List of triggers in curve component
	/// </summary>
	public List<BezierTrigger> triggers = new List<BezierTrigger>();

	/// <summary>
	/// Previous curve. It is not defining connected curve. It simplify moving between curves.
	/// </summary>
	public List<BezierCurve> previousCurves = new List<BezierCurve>();

	/// <summary>
	/// Next curve. It is not defining connected curve. It simplify moving between curves.
	/// </summary>
	public List<BezierCurve> nextCurves = new List<BezierCurve>();

	/// <summary>
	/// If autoUpdate is off, some of changes during play mode will be not updated automaticly. Improves efficiency.
	/// If you want manually update curve, invoke VerifyCurve().
	/// </summary>
	public bool autoUpdate = true;
	#endregion

	#region Methods
	/// <summary>
	/// Active triggers in selected point of curve.
	/// </summary>
	/// <param name="t">Position on curve [0.1]</param>
	/// <returns>Array of active triggers in selected point</returns>
	public virtual BezierTrigger[] GetActiveTriggers(float t)
	{
		t = Mathf.Clamp01 (t);
		List<BezierTrigger> list = new List<BezierTrigger> ();

		foreach (var item in triggers) 
		{
			if (item.Check (t))
				list.Add (item);
		}
		return list.ToArray();
	}

	/// <summary>
	/// Check if there is a threat of infinity loop.
	/// </summary>
	/// <param name="startCurve">Main curve component</param>
	/// <param name="curves">List of curve components to verify</param>
	/// <returns>True, if infinity loop exist</returns>
	/*public static bool InfinityLoopConnection(BezierCurve startCurve, List<BezierCurve> curves)
	{
		foreach(BezierCurve item in curves)
		{
			if (InfinityLoopConnection (startCurve, item))
				return true;
		}
		return false;
	}*/

	/// <summary>
	/// Check if there is a threat of infinity loop.
	/// </summary>
	/// <param name="startCurve">Main curve component</param>
	/// <param name="curve">Curve component to verify</param>
	/// <returns>True, if infinity loop exist</returns>
	/*public static bool InfinityLoopConnection(BezierCurve startCurve, BezierCurve curve)
	{
		
		if (curve == null)
			return false;

		if (curve.Equals (startCurve)) 
		{
			Debug.LogWarning ("Infinity loop detected");
			return true;
		}

		switch (curve.GetType ().Name)
		{
		case "BezierSingle":
			{
				if (InfinityLoopConnection (startCurve, ((BezierSingle)curve).startConnectedCurve) || InfinityLoopConnection (startCurve, ((BezierSingle)curve).endConnectedCurve))
					return true;
				break;
			}
		case "BezierSplit":
			{
				if (InfinityLoopConnection (startCurve, ((BezierSplit)curve).relatedCurve))
					return true;
				break;
			}
		case "BezierLerp":
			{
				if (InfinityLoopConnection (startCurve, ((BezierLerp)curve).startCurve) || InfinityLoopConnection (startCurve, ((BezierLerp)curve).endCurve))
					return true;
				break;
			}
		case "BezierSpline":
			{
				if (InfinityLoopConnection (startCurve, ((BezierSpline)curve).curves))
					return true;
				break;
			}
		}

		return false;
	}*/
	#endregion

	#region Abstract Methods
	/// <summary>
	/// Get acceleration in selected point of curve.
	/// </summary>
	/// <param name="t">Position on curve [0,1]</param>
	/// <returns>Acceleration vector in global transform.</returns>
	public abstract Vector3 GetAcceleration (float t);

	/// <summary>
	/// Get end node of curve
	/// </summary>
	public abstract BezierNode GetEndNode ();

	/// <summary>
	/// Get forward direction in selected point of curve.
	/// </summary>
	/// <param name="t">Position on curve [0,1]</param>
	/// <returns>Forward direction vector in global transform.</returns>
	public abstract Vector3 GetForward(float t);

	/// <summary>
	/// Get distance length from start to selected point on curve.
	/// </summary>
	/// <param name="t">Position on curve [0,1]</param>
	/// <returns>Length of curve element.</returns>
	public abstract float GetLength (float t);

	/// <summary>
	/// Get position in selected point of curve.
	/// </summary>
	/// <param name="t">Position on curve [0,1]</param>
	/// <returns>Point coordinates in global transform.</returns>
	public abstract Vector3 GetPosition (float t);

	/// <summary>
	/// Get rotation around curve in selected point.
	/// </summary>
	/// <param name="t">Position on curve [0,1]</param>
	/// <returns>Rotation in angles.</returns>
	public abstract float GetRotation (float t);

	/// <summary>
	/// Get start node of curve
	/// </summary>
	public abstract BezierNode GetStartNode ();

	/// <summary>
	/// Get up direction in selected point of curve.
	/// </summary>
	/// <param name="t">Position on curve [0,1]</param>
	/// <param name="ignoreInflectionPoints">Ignore inflection points of curve. Improve rotation continuity.</param>
	/// <param name="ignoreRotation">Ignore manual rotation changes. Get clear up direction vector.</param>
	/// <returns>Up direction vector in global transform.</returns>
	public abstract Vector3 GetUp(float t,bool includeInflectionPoints = false, bool ignoreRotation = false);

	/// <summary>
	/// Get velocity in selected point of curve.
	/// </summary>
	/// <param name="t">Position on curve [0,1]</param>
	/// <returns>Velocity vector in global transform.</returns>
	public abstract Vector3 GetVelocity (float t);

	public abstract List<float> IntersectionLine (Vector3 position, Vector3 direction,float accuracy = 0.01f, float floatAccuracy = 0.00001f);

	public abstract List<float> IntersectionPlane (Vector3 position, Vector3 normal, float floatAccuracy = 0.00001f);

	/// <summary>
	/// Change direction of curve in component.
	/// </summary>
	public abstract void Reverse ();
	#endregion

	#region EditorOnly
	#if UNITY_EDITOR
	[SerializeField]
	protected int mainToolbarIndex = 0;
	[SerializeField]
	protected bool includeInflectionPoints = false;
	[SerializeField]
	protected bool ignoreRotation = false;
	[SerializeField]
	protected bool showCurve = true;
	[SerializeField]
	protected bool showDirection = true;
	[SerializeField]
	protected bool showUp = true;
	[SerializeField]
	protected bool showNeighbourhood = true;
	[SerializeField]
	protected bool showTriggers = true;
	[SerializeField]
	protected bool showGizmoParameters = false;
	public Color gizmoColor = Color.white;
	[SerializeField]
	protected int resolution = 20;
	#endif
	#endregion
}
