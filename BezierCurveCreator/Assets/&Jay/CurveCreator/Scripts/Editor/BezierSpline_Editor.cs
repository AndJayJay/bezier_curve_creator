﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

[CustomEditor(typeof(BezierSpline),true)]
class BezierSpline_Editor : BezierCurve_Editor 
{
	#region Attributes
	Dictionary<BezierCurve,string> tempList = new Dictionary<BezierCurve,string>();
	#endregion

	#region Methods
	public override void DrawAll()
	{
		BezierSpline bezierSpline = (BezierSpline)serializedObject.targetObject;

		tempList.Clear ();

		int i = 0;
		foreach (var curve in bezierSpline.curves) 
		{
			if(curve != null)
			if (!tempList.ContainsKey (curve)) 
			{
				tempList.Add(curve,i.ToString()+" ");
			}
			else
			{
				tempList [curve] += i.ToString () + " ";
			}
			i++;
		}

		for(i = 0;i < bezierSpline.curves.Count;i++)
		{
			Handles.color = Color.white;
			if (i > 0 && bezierSpline.curves[i-1]!= null && bezierSpline.curves[i-1]!= null) 
				Handles.DrawLine (bezierSpline.curves[i-1].GetPosition(1f), bezierSpline.curves[i].GetPosition(0f));
		}
		SerializedProperty property;

		foreach (var item in tempList) 
		{
			DrawLabelRectangle(item.Value, item.Key.GetPosition (0.5f),new Rect(-30,20,12*item.Value.Length/2,20),  item.Key.gizmoColor);

			property = serializedObject.FindProperty ("showTriggers");
			if (property.boolValue && !serializedObject.FindProperty ("forceTriggers").boolValue)
				DrawTriggers (item.Key);
			
			property = serializedObject.FindProperty ("showUp");
			if(property.boolValue)
				DrawRotation (item.Key);
			property = serializedObject.FindProperty ("showDirection");
			if(property.boolValue)
				DrawDirection (item.Key);
			property = serializedObject.FindProperty ("showCurve");
			if(property.boolValue)
				DrawCurve (item.Key);
		}

		property = serializedObject.FindProperty ("showTriggers");
		if (property.boolValue && serializedObject.FindProperty ("forceTriggers").boolValue) 
		{
			DrawSplineTriggers (bezierSpline);
		}
	}

	public void DrawSplineTriggers(BezierSpline bezierSpline)
	{
		int triggerCount = bezierSpline.triggers.Count;
		int ins = 0;
		float curveStep = 1f / bezierSpline.curves.Count;
		SerializedObject serializedCurve; 

		foreach (var item in bezierSpline.triggers) 
		{
			if (item == null)
				continue;

			Handles.color = new Color ((float)ins/((float)triggerCount-1f), 0f,(1f - (float)ins/((float)triggerCount-1f)), 1f);
			ins++;

			float tempStart = Mathf.Repeat (item.start, curveStep);
			int startCurveNumber;
			if (item.start == 1) 
			{
				startCurveNumber = bezierSpline.curves.Count - 1;
				tempStart = bezierSpline.curves.Count;
			} else 
			{
				startCurveNumber = Convert.ToInt32 ((item.start - tempStart)*bezierSpline.curves.Count);
				tempStart *= bezierSpline.curves.Count;
			}

			float tempEnd = Mathf.Repeat (item.end, curveStep);
			int endCurveNumber;
			if (item.end == 1) 
			{
				endCurveNumber = bezierSpline.curves.Count - 1;
				tempEnd = bezierSpline.curves.Count;
			}
			else
			{
				endCurveNumber = Convert.ToInt32 ((item.end - tempEnd)*bezierSpline.curves.Count);
				tempEnd *= bezierSpline.curves.Count;
			}

			int resolution;
			for (int i = startCurveNumber; i <= endCurveNumber; i++) 
			{
				float start = tempStart, end=tempEnd;
				if (bezierSpline.curves [i] == null)
					continue;
				serializedCurve = new SerializedObject (bezierSpline.curves [i]);
				resolution = serializedCurve.FindProperty ("resolution").intValue;
				if (i > startCurveNumber)
					start = 0;
				if (i < endCurveNumber)
					end = 1;

				DrawLadder (bezierSpline.curves [i], resolution, start, end);
			}
		}
	}

	public override void DrawGizmoToolbar()
	{
		serializedObject.FindProperty("showGizmoParameters").boolValue = EditorGUILayout.Foldout(serializedObject.FindProperty("showGizmoParameters").boolValue,"Gizmo Parameters");

		if (serializedObject.FindProperty ("showGizmoParameters").boolValue) 
		{
			base.DrawGizmoToolbar ();
		}
	}

	public override void DrawInvidualToolbar()
	{
		EditorGUI.BeginChangeCheck ();

		var temp = ((BezierSpline)serializedObject.targetObject).curves;

		EditorGUILayout.PropertyField (serializedObject.FindProperty ("_curves"),new GUIContent("Curves","Inside curves. Curves used to create spline. Order of curves in list corresponds to the order in spline."),true);
		if (EditorGUI.EndChangeCheck ()) 
		{
		//	if(BezierCurve.InfinityLoopConnection ((BezierSpline)serializedObject.targetObject,((BezierSpline)serializedObject.targetObject).curves))
				((BezierSpline)serializedObject.targetObject).curves = temp;
		}

	}

	public override void DrawRotationToolbar()
	{
		/*EditorGUILayout.PropertyField (serializedObject.FindProperty ("forceRotation"));
		if (!serializedObject.FindProperty ("forceRotation").boolValue)
			GUI.enabled = false;

		EditorGUI.indentLevel++;
		base.DrawRotationToolbar ();
		EditorGUI.indentLevel--;
		GUI.enabled = true;*/
	}

	public override void DrawTriggersToolbar()
	{

		EditorGUILayout.PropertyField (serializedObject.FindProperty ("forceTriggers"),new GUIContent("Force Triggers","Ignore triggers from internal curves and use local triggers."));
		if (!serializedObject.FindProperty ("forceTriggers").boolValue)
			GUI.enabled = false;

		EditorGUI.indentLevel++;
		base.DrawTriggersToolbar ();
		EditorGUI.indentLevel--;
		GUI.enabled = true;
	}

	public override string GetName()
	{
		return "Spline";
	}
	#endregion













}
