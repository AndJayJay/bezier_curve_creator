﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(BezierSplit),true)]
class BezierSplit_Editor : BezierSingle_Editor  
{

	#region Methods
	public override void DrawAll()
	{
		BezierSplit temp = ((BezierSplit)serializedObject.targetObject);

		SerializedProperty property = serializedObject.FindProperty ("showCurve");
		if(property.boolValue)
			DrawReferenceLine (temp);
		property = serializedObject.FindProperty ("showTriggers");
		if(property.boolValue)
			DrawTriggers (temp);
		property = serializedObject.FindProperty ("showUp");
		if(property.boolValue)
			DrawRotation (temp);
		property = serializedObject.FindProperty ("showDirection");
		if(property.boolValue)
			DrawDirection (temp);
		property = serializedObject.FindProperty ("showCurve");
		if(property.boolValue)
			DrawCurve (temp);
	}

	public override void DrawInvidualToolbar()
	{
		EditorGUILayout.PropertyField (serializedObject.FindProperty ("relatedCurve"),new GUIContent("Related Curve", "Related curve to split."));
		EditorGUILayout.PropertyField (serializedObject.FindProperty ("space"),new GUIContent("Space", "Space of curves splitting."), true);
		var temp = (BezierSplit)serializedObject.targetObject;

		EditorGUI.BeginChangeCheck ();
		float start = EditorGUILayout.Slider (new GUIContent("Start", "Start point of splitting."),temp.startT, 0f, 1f);
		float end = EditorGUILayout.Slider (new GUIContent("End", "End point of splitting."),temp.endT, 0f, 1f);

		if (EditorGUI.EndChangeCheck ()) 
		{
			Undo.RecordObject (serializedObject.targetObject, "Change Bezier Split range");
			temp.startT = start;
			temp.endT = end;
			EditorUtility.SetDirty (serializedObject.targetObject);
		}

		EditorGUILayout.BeginHorizontal ();
		GUILayout.FlexibleSpace ();
		if (GUILayout.Button (new GUIContent("   Convert to BezierSingle   ","Generate new BezierSingle object from this curve."))) 
		{
			Undo.RecordObject (serializedObject.targetObject, "Conver BezierSplit");
			((BezierSplit)serializedObject.targetObject).ConvertToBezierSingle (false);
			EditorUtility.SetDirty (serializedObject.targetObject);
		}
		EditorGUILayout.EndHorizontal ();
	}

	public override void DrawRotationToolbar()
	{
		EditorGUILayout.PropertyField (serializedObject.FindProperty ("forceRotation"),new GUIContent("Force Rotation","Ignore rotation interpolated from curves and use local rotation."));
		if (!serializedObject.FindProperty ("forceRotation").boolValue)
			GUI.enabled = false;

		EditorGUI.indentLevel++;
		base.DrawRotationToolbar ();
		EditorGUI.indentLevel--;
		GUI.enabled = true;
	}

	public override string GetName()
	{
		return "Split";
	}
	#endregion

}