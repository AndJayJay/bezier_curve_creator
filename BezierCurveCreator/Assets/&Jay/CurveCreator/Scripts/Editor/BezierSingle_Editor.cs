﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(BezierSingle),true)]
class BezierSingle_Editor : BezierCurve_Editor 
{
	#region Attributes
	int selectedPointIndex = -1;
	Quaternion rotation;
	Transform transform;
	#endregion

	#region Methods
	void ControlPoints(BezierSingle singleCurve)
	{		
		transform = singleCurve.transform;
		rotation = Tools.pivotRotation == PivotRotation.Local ? transform.rotation : Quaternion.identity;

		Color tempColor = Color.white;
		int tempType;
		for (int i = 0; i < 4; i++) 
		{
			if (i < 2)
				tempType = (int)singleCurve.startConnectionType;
			else
				tempType = (int)singleCurve.endConnectionType;

			if ((i == 0 || i == 3) && tempType == 1 && Tools.current != Tool.Rotate) 
			{
				if(selectedPointIndex == i)
					selectedPointIndex = -1;
				continue;	
			}

			if ((i == 0 || i == 3) && tempType > 1) 
			{
				if(selectedPointIndex == i)
					selectedPointIndex = -1;
				continue;	
			}

			if ((i == 1 || i == 2) && tempType == 2 || tempType == 3)
				tempColor = Color.cyan;
			else
				tempColor = Color.white;

			if ((i == 1 || i == 2) && tempType == 4 || tempType==5)
			{
				if(selectedPointIndex == i)
					selectedPointIndex = -1;
				continue;	
			}

			selectedPointIndex = CreateHandle (i,rotation, tempColor);
		}

		if (selectedPointIndex != -1) 
		{
			EditorGUI.BeginChangeCheck ();
			switch (Tools.current) 
			{
			case Tool.Rotate:
				{
					SelectedRotationTool ();
					break;
				}
			default:
				{
					SelectedPositionTool ();
					break;
				}
			}

		}
	}

	int CreateHandle(int index,Quaternion rotation,Color color)
	{
		float handleSize = 0.04f;
		float pickSize = 0.06f;

		Handles.color = color;
		Vector3 point = transform.TransformPoint (((BezierSingle)serializedObject.targetObject).GetCurve()[index]);
		float size = HandleUtility.GetHandleSize (point);
		if (Handles.Button (point, rotation, size*handleSize, size*pickSize, Handles.DotHandleCap)) 
			return index;
		else
			return selectedPointIndex;
	}

	Vector3 CreatePositionGizmo(int index,Quaternion rotation)
	{		
		return transform.InverseTransformPoint (Handles.DoPositionHandle (transform.TransformPoint (((BezierSingle)serializedObject.targetObject).GetCurve()[index]), rotation));
	}

	Quaternion CreateRotationGizmo(BezierNode node)
	{		
		return Handles.DoRotationHandle (rotation*Quaternion.Euler(node.rotation), transform.TransformPoint (node.mainPosition));
	}

	public override void DrawAll()
	{
		BezierSingle temp = ((BezierSingle)serializedObject.targetObject);

		SerializedProperty property = serializedObject.FindProperty ("showCurve");
		if(property.boolValue)
			DrawReferenceLine (temp);
		property = serializedObject.FindProperty ("showTriggers");
		if(property.boolValue)
			DrawTriggers (temp);
		property = serializedObject.FindProperty ("showUp");
		if(property.boolValue)
			DrawRotation (temp);
		property = serializedObject.FindProperty ("showDirection");
		if(property.boolValue)
			DrawDirection (temp);
		property = serializedObject.FindProperty ("showCurve");
		if(property.boolValue)
			DrawCurve (temp);

		ControlPoints (temp);
	}

	public override void DrawGizmoToolbar()
	{
		serializedObject.FindProperty("showGizmoParameters").boolValue = EditorGUILayout.Foldout(serializedObject.FindProperty("showGizmoParameters").boolValue,"Gizmo Parameters");

		if (serializedObject.FindProperty ("showGizmoParameters").boolValue) 
		{
			EditorGUILayout.PropertyField (serializedObject.FindProperty ("gizmoColor"), true);
			EditorGUILayout.PropertyField (serializedObject.FindProperty ("resolution"),new GUIContent("Resolution","Resolution of curve in inspector."), true);
			EditorGUILayout.PropertyField (serializedObject.FindProperty ("includeInflectionPoints"),new GUIContent("Include Inflection Points","Include inflection points in up vector gizmo."));
			EditorGUILayout.PropertyField (serializedObject.FindProperty ("ignoreRotation"),new GUIContent("Ignore Rotation","Ignore rotation points in up vector gizmo."));
			base.DrawGizmoToolbar ();
		}
	}
		
	public override void DrawInvidualToolbar()
	{
		SerializedProperty connectionType,connectionPoint, connectedCurve, nodeProperty;

		serializedObject.FindProperty("showStartNode").boolValue = EditorGUILayout.Foldout(serializedObject.FindProperty("showStartNode").boolValue,new GUIContent("Start Node","Start node of curve."));

		if (serializedObject.FindProperty ("showStartNode").boolValue) 
		{
			connectedCurve = serializedObject.FindProperty ("startConnectedCurve");
			connectionType = serializedObject.FindProperty ("startConnectionType");
			connectionPoint = serializedObject.FindProperty ("startConnectionPoint");
			nodeProperty = serializedObject.FindProperty ("curve.startNode");
			DrawNodeToolbar (connectionType,connectionPoint,connectedCurve,nodeProperty);
		}

		serializedObject.FindProperty("showEndNode").boolValue = EditorGUILayout.Foldout(serializedObject.FindProperty("showEndNode").boolValue,new GUIContent("End Node","End node of curve."));

		if (serializedObject.FindProperty ("showEndNode").boolValue) 
		{
			connectedCurve = serializedObject.FindProperty ("endConnectedCurve");
			connectionType = serializedObject.FindProperty ("endConnectionType");
			connectionPoint = serializedObject.FindProperty ("endConnectionPoint");
			nodeProperty = serializedObject.FindProperty ("curve.endNode");
			DrawNodeToolbar (connectionType,connectionPoint, connectedCurve, nodeProperty);
		}


		EditorGUILayout.BeginHorizontal ();
		GUILayout.FlexibleSpace ();
		if (GUILayout.Button (new GUIContent("   Reverse   ","Change curve direction."))) 
		{
			Undo.RecordObject (serializedObject.targetObject, "Reverse BezierSingle");
			((BezierSingle)serializedObject.targetObject).Reverse ();
			EditorUtility.SetDirty (serializedObject.targetObject);
		}

		EditorGUILayout.EndHorizontal ();
	}

	void DrawNodeToolbar(SerializedProperty connectionTypeProperty,SerializedProperty connectionPointProperty,SerializedProperty connectedCurveProperty,SerializedProperty nodeProperty)
	{
		EditorGUILayout.PropertyField (connectedCurveProperty,new GUIContent("Connection Curve","Relative curve to connect node."));
		EditorGUI.indentLevel++;
		EditorGUILayout.PropertyField (connectionPointProperty,new GUIContent("Connection Point","The relative curve node to connect to node."));
		EditorGUILayout.PropertyField (connectionTypeProperty,new GUIContent("Connection Type","Method of node connection."));
		EditorGUI.indentLevel--;
		GUI.enabled = false;

		if (connectionTypeProperty.intValue == 0) 
			GUI.enabled = true;
		EditorGUILayout.PropertyField (nodeProperty.FindPropertyRelative("mainPosition"),new GUIContent("Main Position","Main position of node."));
		if(Mathf.Abs(connectionTypeProperty.intValue) <= 1)
			GUI.enabled = true;
		EditorGUILayout.PropertyField (nodeProperty.FindPropertyRelative ("rotation"),new GUIContent("Rotation","Rotation of node."));
		Vector3 temp = nodeProperty.FindPropertyRelative ("rotation").vector3Value;
		temp.x = Mathf.Repeat (temp.x, 360f);
		temp.y = Mathf.Repeat (temp.y, 360f);
		temp.z = Mathf.Repeat (temp.z, 360f);
		nodeProperty.FindPropertyRelative ("rotation").vector3Value = temp;
		if(Mathf.Abs(connectionTypeProperty.intValue) <= 3)
			GUI.enabled = true;
		EditorGUILayout.PropertyField (nodeProperty.FindPropertyRelative ("length"),new GUIContent("Length","Length between node points."));
		GUI.enabled = true;
	}
			
	public override void DrawRotationToolbar()
	{
		EditorGUILayout.CurveField (serializedObject.FindProperty ("rotationCurve"), Color.green, new Rect (0f, -720f, 1f, 1440f),new GUIContent("Rotation","Defined rotation change from basic up vector."));
	}

	public override string GetName()
	{
		return "Curve";
	}

	void SelectedPositionTool()
	{
		Vector3 tempPoint = Vector3.zero;
		tempPoint = CreatePositionGizmo (selectedPointIndex,rotation);
		if (EditorGUI.EndChangeCheck ()) 
		{
			Undo.RecordObject (serializedObject.targetObject, "Move Point");
			((BezierSingle)serializedObject.targetObject).GetCurve()[selectedPointIndex] = tempPoint;
		}
	}

	void SelectedRotationTool()
	{
		BezierNode editedNode = null;

		switch (selectedPointIndex) 
		{
		case 0:
			{
				editedNode = ((BezierSingle)serializedObject.targetObject).GetStartNode();
				break;		
			}
		case 1:
			{
				SelectedPositionTool ();
				return;
			}
		case 2:
			{
				SelectedPositionTool ();
				return;
			}
		case 3:
			{
				editedNode = ((BezierSingle)serializedObject.targetObject).GetEndNode();
				break;		
			}
		}		

		Vector3 tempRotation = CreateRotationGizmo (editedNode).eulerAngles; 

		if (EditorGUI.EndChangeCheck ()) 
		{
			Undo.RecordObject ((serializedObject.targetObject), "Rotate Point");
			editedNode.rotation = tempRotation - rotation.eulerAngles;
		}
	}

	#endregion























}
