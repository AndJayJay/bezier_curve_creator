﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(BezierLerp),true)]
class BezierLerp_Editor : BezierSingle_Editor  
{
	#region Methods
	public override void DrawAll()
	{
		BezierLerp temp = ((BezierLerp)serializedObject.targetObject);

		SerializedProperty property = serializedObject.FindProperty ("showCurve");
		if(property.boolValue)
			DrawReferenceLine (temp);
		property = serializedObject.FindProperty ("showTriggers");
		if(property.boolValue)
			DrawTriggers (temp);
		property = serializedObject.FindProperty ("showUp");
		if(property.boolValue)
			DrawRotation (temp);
		property = serializedObject.FindProperty ("showDirection");
		if(property.boolValue)
			DrawDirection (temp);
		property = serializedObject.FindProperty ("showCurve");
		if(property.boolValue)
			DrawCurve (temp);
	}

	public override void DrawInvidualToolbar()
	{
		BezierLerp temp = (BezierLerp)serializedObject.targetObject;

		EditorGUILayout.PropertyField (serializedObject.FindProperty ("startCurve"),new GUIContent("Start Curve", "Start curve of interpolation."), true);
		EditorGUILayout.PropertyField (serializedObject.FindProperty ("endCurve"),new GUIContent("End Curve", "End curve of interpolation."), true);
		EditorGUILayout.PropertyField (serializedObject.FindProperty ("space"),new GUIContent("Space", "Space of curves interpolation."), true);

		EditorGUI.BeginChangeCheck ();
		var tempLerpT = EditorGUILayout.Slider (new GUIContent("Lerp T","Weight of interpolation"),temp.lerpT, 0f, 1f);
		var tempLerpType = (BezierLerp.LerpType)EditorGUILayout.EnumPopup (new GUIContent("Type","Type of interpolations."), temp.lerpType);
		var tempLerpDirection = (BezierLerp.LerpDirection)EditorGUILayout.EnumPopup (new GUIContent("Lerp Direction","Direction, between which nodes, curves are interpolated."), temp.lerpDirection);
		if (EditorGUI.EndChangeCheck ()) 
		{
			Undo.RecordObject (serializedObject.targetObject, "Change Bezier Lerp Parameters");
			temp.lerpT = tempLerpT;
			temp.lerpType = tempLerpType;
			temp.lerpDirection = tempLerpDirection;
			EditorUtility.SetDirty (serializedObject.targetObject);
		}

		EditorGUILayout.BeginHorizontal ();
		GUILayout.FlexibleSpace ();
		if (GUILayout.Button (new GUIContent("   Convert to BezierSingle   ","Generate new BezierSingle object from this curve."))) 
		{
			Undo.RecordObject (serializedObject.targetObject, "Conver BezierLerp");
			((BezierLerp)serializedObject.targetObject).ConvertToBezierSingle (false);
			EditorUtility.SetDirty (serializedObject.targetObject);
		}
		EditorGUILayout.EndHorizontal ();
	}

	public override void DrawRotationToolbar()
	{
		EditorGUILayout.PropertyField (serializedObject.FindProperty ("forceRotation"),new GUIContent("Force Rotation","Ignore rotation interpolated from curves and use local rotation."));
		if (!serializedObject.FindProperty ("forceRotation").boolValue)
			GUI.enabled = false;

		EditorGUI.indentLevel++;
		base.DrawRotationToolbar ();
		EditorGUI.indentLevel--;
		GUI.enabled = true;
	}

	public override string GetName()
	{
		return "Lerp";
	}
	#endregion
}