﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

abstract class BezierCurve_Editor : Editor {

	#region Methods
	public void DrawCurve(BezierCurve bezierCurve)
	{
		int resolution = serializedObject.FindProperty ("resolution").intValue;
		Vector3 point;

		for (float i = 0; i < resolution; i++) 
		{	
			Handles.color = new Color (1f - 0.8f * i / ((float)resolution), 1f - 0.8f * i / ((float)resolution), 1f - 0.8f * i / ((float)resolution));

			point = bezierCurve.GetPosition (i/((float)resolution));
			Handles.DrawLine (point, bezierCurve.GetPosition ((i+1f)/((float)resolution)));
		}
	}

	public void DrawDirection(BezierCurve bezierCurve)
	{
		int resolution = serializedObject.FindProperty ("resolution").intValue;

		Handles.color = Color.blue;
		float directionScale = 0.5f;

		Vector3 point,direction;
		for (float i = 0; i <= resolution; i++) 
		{			
			point = bezierCurve.GetPosition (i/((float)resolution));
			direction = bezierCurve.GetForward (i / ((float)resolution));
			Handles.DrawLine (point, point + direction * directionScale);
		}
	}

	public virtual void DrawGizmoToolbar ()
	{
		EditorGUILayout.PropertyField (serializedObject.FindProperty ("showCurve"),new GUIContent("Show Curve","Show curve direction gizmo."));
		EditorGUILayout.PropertyField (serializedObject.FindProperty ("showDirection"),new GUIContent("Show Direction","Show forward vector gizmo."));
		EditorGUILayout.PropertyField (serializedObject.FindProperty ("showUp"),new GUIContent("Show Up","Show up vector gizmo."));
		EditorGUILayout.PropertyField (serializedObject.FindProperty ("showTriggers"),new GUIContent("Show Triggers","Show triggers added to curve on gizmo."));
		EditorGUILayout.PropertyField (serializedObject.FindProperty ("showNeighbourhood"),new GUIContent("Show Neighbourhood","Sign next/previous curves in inspector."));
	}

	public void DrawLabelRectangle(string text, Vector3 globalPosition,Rect size, Color color)
	{
		Vector3[] Vectors = new Vector3[5];
		Vector3 temp;
		GUIStyle style = new GUIStyle ();

		temp = SceneView.currentDrawingSceneView.camera.WorldToScreenPoint (globalPosition);
		Vectors [0] = SceneView.currentDrawingSceneView.camera.ScreenToWorldPoint(temp+new Vector3(size.xMin,size.yMax,0));
		Vectors [1] = SceneView.currentDrawingSceneView.camera.ScreenToWorldPoint(temp+new Vector3(size.xMax,size.yMax,0));
		Vectors [2] = SceneView.currentDrawingSceneView.camera.ScreenToWorldPoint(temp+new Vector3(size.xMax,size.yMin,0));
		Vectors [3] = SceneView.currentDrawingSceneView.camera.ScreenToWorldPoint(temp+new Vector3(size.xMin,size.yMin,0));
		Vectors [4] = SceneView.currentDrawingSceneView.camera.ScreenToWorldPoint(temp+new Vector3(size.xMin+5,size.yMax,0));

		Handles.DrawSolidRectangleWithOutline(Vectors,new Color(color.r,color.g,color.b,0.3f),new Color(color.r,color.g,color.b,0.6f));
		style.normal.textColor = new Color(1f-color.r,1f-color.g,1f-color.b,1f);
		Handles.Label (Vectors[4],text,style);
	}

	public virtual void DrawNeighbourhoodToolbar()
	{
		EditorGUILayout.PropertyField (serializedObject.FindProperty ("previousCurves"),new GUIContent("Previous Curves","List of previous curves. It haven't influence for curve working."),true);
		EditorGUILayout.PropertyField (serializedObject.FindProperty ("nextCurves"),new GUIContent("Next Curves","List of next curves. It haven't influence for curve working."),true);
	}

	public void DrawReferenceLine(BezierCurve curveBezier)
	{
		Handles.color = Color.grey;


		Vector3 tempStartMain, tempStartRef, tempEndRef, tempEndMain;
		tempStartMain = curveBezier.transform.TransformPoint (curveBezier.GetStartNode().mainPosition);
		tempStartRef = curveBezier.transform.TransformPoint (curveBezier.GetStartNode().referencePosition);
		tempEndRef = curveBezier.transform.TransformPoint (curveBezier.GetEndNode().referencePosition);
		tempEndMain = curveBezier.transform.TransformPoint (curveBezier.GetEndNode().mainPosition);

		Handles.DrawLine (tempStartMain, tempStartRef);
		Handles.DrawLine (tempEndRef, tempEndMain);
	}
		
	public void DrawRotation(BezierCurve bezierCurve)
	{
		int resolution = serializedObject.FindProperty ("resolution").intValue;
		float directionScale = 0.5f;

		Vector3 point,up;
		for (float i = 0; i <= resolution; i++) 
		{			
			point = bezierCurve.GetPosition (i/((float)resolution));
			up = bezierCurve.GetUp (i / ((float)resolution),serializedObject.FindProperty ("includeInflectionPoints").boolValue,serializedObject.FindProperty ("ignoreRotation").boolValue);

			Handles.color = Color.green;
			Handles.DrawLine (point, point + up*directionScale*0.5f);
		}
	}

	public void DrawTriggers(BezierCurve bezierCurve)
	{
		int resolution = serializedObject.FindProperty ("resolution").intValue;

		int triggerCount = bezierCurve.triggers.Count;
		int ins = 0;

		foreach (var item in bezierCurve.triggers) 
		{
			if (item == null)
				continue;

			Handles.color = new Color ((float)ins/((float)triggerCount-1f), 0f,(1f - (float)ins/((float)triggerCount-1f)), 1f);
			ins++;

			DrawLadder (bezierCurve, resolution, item.start, item.end);
		}
	}

	public void DrawLadder(BezierCurve bezierCurve, int resolution, float start, float end)
	{
		if (bezierCurve == null)
			return;

		Vector3 point, pointUp1, pointDown1,pointUp2, pointDown2;
		Vector3 direction, cross;
		float actualPos;
		float nextPos;

		for (float i = 0; i < resolution; i++) 
		{	
			actualPos = i /((float)resolution);
			nextPos = (i + 1f) /((float)resolution);

			if (actualPos < start) 
			{
				if (nextPos <= start) 
				{
					continue;
				} 
				else 
				{
					actualPos = start;
				}

			}
			if (nextPos > end) 
			{
				if (actualPos >= end) 
				{
					continue;
				} 
				else 
				{
					nextPos = end;
				}

			}

			point = bezierCurve.GetPosition (actualPos);
			direction = bezierCurve.GetForward (actualPos);
			cross = Vector3.Cross (bezierCurve.GetUp(actualPos), direction);
			pointUp1 = point + cross * 0.1f;
			pointDown1 = point - cross * 0.1f;

			point = bezierCurve.GetPosition (nextPos);
			direction = bezierCurve.GetForward (nextPos);
			cross = Vector3.Cross (bezierCurve.GetUp(actualPos), direction);
			pointUp2 = point + cross * 0.1f;
			pointDown2 = point - cross * 0.1f;

			Handles.DrawLine (pointUp1, pointDown2);
			Handles.DrawLine (pointUp2, pointDown1);
		}
	}

	public virtual void DrawTriggersToolbar ()
	{
		EditorGUILayout.PropertyField (serializedObject.FindProperty ("triggers"),new GUIContent("Triggers","List of triggers on curve."),true);

		BezierCurve temp = (BezierCurve)serializedObject.targetObject;

		float temp2;
		foreach (var item in temp.triggers) 
		{
			temp2 = item.start;
			item.start = item.end;
			item.start = temp2;
			temp2 = item.end;
			item.end = item.start;
			item.end = temp2;
		}

	}

	public override void OnInspectorGUI ()
	{
		serializedObject.Update ();
		SerializedProperty mainToolBarProperty = serializedObject.FindProperty("mainToolbarIndex");


		EditorGUILayout.BeginVertical ();
		EditorGUILayout.PropertyField (serializedObject.FindProperty ("autoUpdate"),new GUIContent("Auto Update","Automatically checking curve changes in PlayMode. To manually update curve use VerifyCurve(). Off to improve efficiency."));
		mainToolBarProperty.intValue = GUILayout.Toolbar (mainToolBarProperty.intValue, new string[]{ "General",GetName() });
		EditorGUILayout.Space ();
		EditorGUI.indentLevel++;
		switch (mainToolBarProperty.intValue ) {
		case 1:
			{

				DrawInvidualToolbar ();
				break;
			}
		case 0:
			{
				DrawRotationToolbar ();
				EditorGUILayout.Space ();
				DrawNeighbourhoodToolbar ();
				EditorGUILayout.Space ();
				DrawTriggersToolbar ();
				EditorGUILayout.Space ();
				DrawGizmoToolbar ();
				break;
			}
		}
		EditorGUI.indentLevel--;
		EditorGUILayout.EndVertical ();
		serializedObject.ApplyModifiedProperties ();

	}

	public void OnSceneGUI()
	{
		DrawAll ();
		if(serializedObject.FindProperty ("showNeighbourhood").boolValue)			
			ShowNeighbourhood ();
	}

	public void ShowNeighbourhood()
	{
		Handles.color = Color.white;

		foreach (var item in ((BezierCurve)serializedObject.targetObject).nextCurves) {
			if (item == null || !item.gameObject.activeSelf)
				continue;

			DrawLabelRectangle ("Next Curve", item.GetPosition (0.5f), new Rect (-30, 0, 75, 20), item.gizmoColor);

		}
		foreach (var item in ((BezierCurve)serializedObject.targetObject).previousCurves) {
			if (item == null || !item.gameObject.activeSelf)
				continue;

			DrawLabelRectangle ("Previous Curve", item.GetPosition (0.5f), new Rect (-40, -20, 95, 20), item.gizmoColor);

		}

	}
	#endregion

	#region Abstract Methods
	public abstract void DrawInvidualToolbar ();
	public abstract void DrawRotationToolbar ();
	public abstract void DrawAll();
	public abstract string GetName();
	#endregion

}
