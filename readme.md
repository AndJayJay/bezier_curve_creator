Projekt assetu do obsługi krzywych Beziera w Unity

Krzywa Beziera jest to krzywa parametryczna opisująca położenie w przestrzeni na podstawie aproksymacji punktów, nazywanych węzłami.
W zależności od liczby węzłów można mówić o krzywej 3,4 i n-tego rzędu (gdzie n oznacza liczbę węzłów).
Połoźenie na krzywej jest opisywane za pomocą parametru t, Gdzie dla wartości t=0, otrzymuje się położenie pierwszego węzła,
natomiast dla t = 1 położenie ostatniego węzła. Wszystkie wartości t w zakresie [0,1] są aproksymacją położeń wszystkich węzłów krzywej.

Jako bazę obsługi krzywych w projekcie wykorzystano krzywe 4-rzędu. Jednakże dla wygody obługi zaimplementowano 4 rodzaje obsługi krzywych:
SimpleCurve - podstawowa krzywa Beziera. Jej krztałt można definiować za pomocą położenia 4 węzłów. Zarówno dla początku krzywej jak i jej końca można wybrać wieszchołek innej krzywej z którą tę, można skleić.
SplitCurve - krzywa stanowiąca wycięty fragment wcześniej stworzonej krzywej.
LerpCurve - krzywa będąca aprokcymacją dwóch innych krzywych.
SplineCurve - krzywa będąca sklejeniem kilku pojedyńczych krzywych.

Każda krzywa ma również zaimplementowane pole Previous/Next Curve's, pozwalające na zdefiniowanie kolejności krzywych. Nie mają one wpływu na działe krzywej, a jedynie zastosowanie organizacyjne.